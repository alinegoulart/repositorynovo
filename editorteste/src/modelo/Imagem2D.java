package modelo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.awt.image.BufferedImage;

public class Imagem2D {

	private String nomeImagem;
    private BufferedImage imagem;
    private boolean imageSelecionada;
    private double posicaoX;
    private double posicaoY;
    private double largura;
    private double altura;

    public Imagem2D() {

        this("", null, false, 0, 0, 0, 0);
    }

    public Imagem2D(String nomeImagem, BufferedImage imagem, boolean imageSelecionada, double posicaoX, double posicaoY, double largura, double altura) {
		
    	super();
		this.nomeImagem = nomeImagem;
		this.imagem = imagem;
		this.imageSelecionada = imageSelecionada;
		this.posicaoX = posicaoX;
		this.posicaoY = posicaoY;
		this.largura = largura;
		this.altura = altura;
	}
    
	public String getNomeImagem() {
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}

	public BufferedImage getImagem() {
		return imagem;
	}

	public void setImagem(BufferedImage imagem) {
		this.imagem = imagem;
	}

	public boolean isImageSelecionada() {
		return imageSelecionada;
	}

	public void setImageSelecionada(boolean imageSelecionada) {
		this.imageSelecionada = imageSelecionada;
	}

	public double getPosicaoX() {
		return posicaoX;
	}

	public void setPosicaoX(double posicaoX) {
		this.posicaoX = posicaoX;
	}

	public double getPosicaoY() {
		return posicaoY;
	}

	public void setPosicaoY(double posicaoY) {
		this.posicaoY = posicaoY;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public void desenharPontos(Graphics2D g2d) {

        int tamanho = 10;

        if(this.imageSelecionada) {

            g2d.setColor(Color.BLACK);

            double x = this.posicaoX - tamanho / 2;
            double y = this.posicaoY - tamanho / 2;
            java.awt.geom.Rectangle2D r = new java.awt.geom.Rectangle2D.Double(x, y, tamanho, tamanho);
            g2d.fill(r);

            x = (this.posicaoX + this.largura) - tamanho / 2;
            y = (this.posicaoY + this.altura) - tamanho / 2;
            r = new java.awt.geom.Rectangle2D.Double(x, y, tamanho, tamanho);
            g2d.fill(r);
        }
    }

    public void desenharImagem(Graphics2D g2d) {

        AffineTransform old = g2d.getTransform();
        
        if(this.imagem != null) {

            g2d.setTransform(AffineTransform.getTranslateInstance(this.posicaoX, this.posicaoY));
            g2d.drawImage(this.imagem, 0, 0, (int)this.largura, (int)this.altura, null);
        }

        g2d.setTransform(old);
    }

    public Double[] obterPontos() {

        return new Point2D.Double[]{new Point2D.Double(this.posicaoX, this.posicaoY), new Point2D.Double(this.posicaoX + this.largura, this.posicaoY + this.altura)};
    }

    public boolean contem(Point2D ponto) {

        return new java.awt.geom.Rectangle2D.Double(this.posicaoX, this.posicaoY, this.largura, this.altura).contains(ponto);
    }
}