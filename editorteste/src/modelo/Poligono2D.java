package modelo;

import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

/**
 * Classe que define um pol�gono
 * @author Raiane
 *
 */
public class Poligono2D extends Figura2D{

	//armazena uma lista de pontos
    private ArrayList<Point2D.Double> listaPontos;
    //classe para desenhar pol�gono
    private GeneralPath generalPath;
    
    /**
     * Cria um pol�gono vazio
     */
    public Poligono2D() {

        this.listaPontos = new ArrayList<Point2D.Double>();
        this.generalPath = new GeneralPath();
    }

    /*
     * Cria um pol�igono a partir de uma lista de pontos
     */
    public Poligono2D(ArrayList<Point2D.Double> listaPontos) {
    	
        this.listaPontos = listaPontos;
        this.generalPath = new GeneralPath();
    }

    /**
     * Retorna a lista de pontos
     * @return lista de pontos
     */
    public ArrayList<Point2D.Double> getListaPontos() {
        return listaPontos;
    }

    /**
     * Atualiza a lista de pontos
     * @param listaPontos lista de pontos
     */
    public void setListaPontos(ArrayList<Point2D.Double> listaPontos) {
        this.listaPontos = listaPontos;
    }

    /**
     * Adiciona um ponto no pol�gono
     * @param ponto ponto a ser adicionado
     */
    public void adicionarPonto(Point2D.Double ponto) {

    	//adiciona o ponto na lista
        this.listaPontos.add(ponto);
    }

    /**
     * Desenha os pontos do pol�gono
     */
    public void desenharPontos(Mat mat) {

        int tamanho = 10;

        if(this.isSelecionado()) {

            Scalar scalar = new Scalar(0, 0, 0);
        	
            //percorre todos os pontos do pol�gono
            for(int i = 0; i < this.listaPontos.size(); i++) {

            	//obt�m o ponto atual
                Point2D pt = this.listaPontos.get(i);
                
                double x = pt.getX() - tamanho / 2;
                double y = pt.getY() - tamanho / 2;
                
                //desenha o ret�ngulo
                Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), scalar, -1);
            }
        }
    }

    /**
     * Desenha o pol�gono
     */
    @Override
    public void desenharFigura(Mat mat) {

    	//obt�m o primeiro ponto do pol�gono
        Point2D primeiroPonto = this.listaPontos.get(0);
        //cria uma lista de pontos
        List<Point> listaPontos = new ArrayList<Point>();
        //cria uma lista de pontos para o opencv
        List<MatOfPoint> listaPontosP = new ArrayList<MatOfPoint>();
        
        this.generalPath = new GeneralPath();
        //mover para o primeiro ponto
        generalPath.moveTo(primeiroPonto.getX(), primeiroPonto.getY());

        //adiciona o primeiro ponto na lista de pontos
        listaPontos.add(new Point(primeiroPonto.getX(), primeiroPonto.getY()));
        
        //percorre os outros pontos do poligono
        for(int i = 1; i < this.listaPontos.size(); i++) {

        	//obt�m o ponto atual
            Point2D pontoAtual = this.listaPontos.get(i);
            //ligo o ponto atual com o anterior
            generalPath.lineTo(pontoAtual.getX(), pontoAtual.getY());
            listaPontos.add(new Point(pontoAtual.getX(), pontoAtual.getY()));
        }

        //cria um vetor a partir da lista de pontos do pol�gono
        Point vetPontos[] = new Point[this.listaPontos.size()];
        
        //percorre a lista de pontos
        for(int i = 0; i < listaPontos.size(); i++) {
        	
        	//copia cada ponto da lista para o vetor
        	vetPontos[i] = listaPontos.get(i);
        }
        
        //adiciona na lista de pontos do opencv o vetor criado
        listaPontosP.add(new MatOfPoint(vetPontos));
        //fecha o pol�gono
        generalPath.closePath();

        //obt�m a cor
        Color cor = this.getCor();
        Scalar scalar = new Scalar(cor.getBlue(), cor.getGreen(), cor.getRed());
     
        //se o pol�gono est� preenchido
        if(this.isPreenchido()) {

        	//desenha o pol�gono preenchido
            Core.polylines(mat, listaPontosP, true, scalar, -1);
        }
        //caso contr�rio
        else {

        	//desenha o pol�gono sem preenchimento
        	Core.polylines(mat, listaPontosP, true, scalar);
        }
    }

    /**
     * Checa se um ponto est� contido no pol�gono
     */
    @Override
    public boolean contem(Point2D ponto) {

    	//chama o m�todo que verifica se o ponto est� contido ou n�o, retorna true ou false
        return this.generalPath.contains(ponto);
    }
    
    /**
     * Retorna um vetor com todos os pontos do poligono
     */
    @Override
    public Point2D.Double[] obterPontos() {

    	//converte a lista de pontos para um vetor e retorna
        return (Point2D.Double[])this.listaPontos.toArray(new Point2D.Double[this.listaPontos.size()]);
    }
}