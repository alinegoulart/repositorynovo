package modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Armazena todos os dados do aplicativo
 * @author Raiane
 *
 */
public class Paint {

	//armazena a lista de figuras
    private List<Figura2D> listaFiguras;
    //armazena a lista de imagens
    private List<Imagem2D> listaImagens;
    
    /**
     * Inicializa o paint
     */
    public Paint() {

    	//cria as listas de figura de imagem
    	this.listaFiguras = new ArrayList<Figura2D>();
    	this.listaImagens = new ArrayList<Imagem2D>();
    }

    /**
     * Adiciona uma figura no aplicativo
     * @param figura figura a ser adicionada
     */
    public void adicionarFigura(Figura2D figura) {

        this.listaFiguras.add(figura);
    }

    /**
     * Remove uma figura do aplicativo
     * @param figura figura a ser removida
     */
    public void removerFigura(Figura2D figura) {
    
    	this.listaFiguras.remove(figura);
    }

    /**
     * Obt�m uma figura dado o seu �ndice 
     * @param indice �ndice da figura
     * @return a figura
     */
    public Figura2D obterFigura(int indice) {

    	//retorna a figura a partir do seu �ndice
        return this.listaFiguras.get(indice);
    }

    /**
     * Remove todas as figuras
     */
    public void removerTodasFiguras() {

    	//limpa a lista de figuras
        this.listaFiguras.clear();
    }

    /**
     * Retorna o total de figuras
     * @return total de figuras
     */
    public int totalFiguras() {

        return this.listaFiguras.size();
    }
    
    /**
     * Adiciona uma imagem no aplicativo
     * @param imagem imagem a ser adicionada
     */
    public void adicionarImagem(Imagem2D imagem) {

        this.listaImagens.add(imagem);
    }

    /**
     * Remove uma imagem do aplicativo
     * @param imagem imagem a ser removida
     */
    public void removerImagem(Imagem2D imagem) {
    
    	this.listaImagens.remove(imagem);
    }

    /**
     * Obt�m uma imagem dado o seu �ndice
     * @param indice �ndice da imagem
     * @return imagem 
     */
    public Imagem2D obterImagem(int indice) {

        return this.listaImagens.get(indice);
    }

    /**
     * Remove todas as imagens
     */
    public void removerTodasImagens() {

    	//limpa a lista de imagens
        this.listaImagens.clear();
    }

    /**
     * Retorna o total de imagens
     * @return total de imagens
     */
    public int totalImagens() {

        return this.listaImagens.size();
    }
}