package modelo;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

/**
 * Classe que representa uma linha, faz heran�a a classe figura
 * @author Raiane
 *
 */
public class Linha2D extends Figura2D {

	//armazena o primeiro ponto da linha
    private Point2D.Double primeiroPonto;
    //armazena o �ltimo ponto da linha
    private Point2D.Double ultimoPonto;
    
    /**
     * Inicializa a linha
     */
    public Linha2D() {

        this(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 0.0));
    }

    /**
     * Inicializa a linha com os pontos
     * @param primeiroPonto primeiro ponto
     * @param ultimoPonto �ltimo ponto
     */
    public Linha2D(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

    	//copia os valores das vari�veis para cada atributo
        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }

    /**
     * Gravar a localiza��o de cada ponto
     * @param primeiroPonto primeiro ponto
     * @param ultimoPonto �ltimo ponto
     */
    public void setLocalizacao(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

    	//copia os valores das vari�veis para cada atributo
        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }
    
    /**
     * Desenha os pontos da linha
     */
    @Override
    public void desenharPontos(Mat mat) {

    	int tamanho = 10;
        
        if(this.isSelecionado()) {

            Scalar cor = new Scalar(0, 0, 0);

            //calcula a posi��o onde ser� desenhado os pontos da linha
            double x = primeiroPonto.getX() - tamanho / 2;
            double y = primeiroPonto.getY() - tamanho / 2;
            //desenha o retangulo
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), cor, -1);
            
            //calcula a posi��o onde ser� desenhado os pontos da linha
            x = ultimoPonto.getX() - tamanho / 2;
            y = ultimoPonto.getY() - tamanho / 2;
            //desenha o retangulo
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), cor, -1);
        }
    }

    /**
     * Desenha a figura
     */
    @Override
    public void desenharFigura(Mat mat) {

    	//pegar a cor da figura
    	Color cor = this.getCor();
    	//define a cor da figura pra desenhar
    	Scalar scalar = new Scalar(cor.getBlue(), cor.getGreen(), cor.getRed());
        
    	//desenha a linha
    	Core.line(mat, new Point(this.primeiroPonto.getX(), this.primeiroPonto.getY()), new Point(this.ultimoPonto.getX(), this.ultimoPonto.getY()), scalar);
    }

    /**
     * Checa se um ponto est� contido na linha
     */
    @Override
    public boolean contem(Point2D ponto) {

    	//crio uma linha 2d com o primeiro e ultimo ponto e checa se o ponto est� contigo retorna true ou false
        return new Line2D.Double(this.primeiroPonto, this.ultimoPonto).contains(ponto);
    }

    /**
     * Retorna o vetor com todos os pontos da linha
     */
    @Override
    public Point2D.Double[] obterPontos() {

    	//cria o vetor com os pontos da linha
        return new Point2D.Double[]{this.primeiroPonto, this.ultimoPonto};
    }
}