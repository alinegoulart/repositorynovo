package modelo;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public class Retangulo2D extends Figura2D {

	private Point2D.Double primeiroPonto;
    private Point2D.Double ultimoPonto;
    
    public Retangulo2D() {

        this(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 0.0));
    }

    public Retangulo2D(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }

    public void setLocalizacao(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }

    @Override
    public void desenharPontos(Mat mat) {

        int tamanho = 10;
        
        if(this.isSelecionado()) {

            Scalar scalar = new Scalar(0, 0, 0);
            
            double x = primeiroPonto.getX() - tamanho / 2;
            double y = primeiroPonto.getY() - tamanho / 2;
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), scalar, -1);

            x = ultimoPonto.getX() - tamanho / 2;
            y = ultimoPonto.getY() - tamanho / 2;
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), scalar, -1);
        }
    }

    /**
     * Desenha o ret�ngulo
     */
    @Override
    public void desenharFigura(Mat mat) {

    	Color cor = this.getCor();
    	Scalar scalar = new Scalar(cor.getRed(), cor.getGreen(), cor.getBlue());
        
    	//se o ret�ngulo est� preenchido
    	if(this.isPreenchido()) {

    		//desenha o ret�ngulo com a cor de preenchimento
    		Core.rectangle(mat, new Point(this.primeiroPonto.getX(), this.primeiroPonto.getY()), new Point(this.ultimoPonto.getX(), this.ultimoPonto.getY()), scalar, -1); //-1 define que o retangulo ser� preenchido
        }
    	//se o ret�ngulo n�o est� preenchido
    	else {

    		//desenha o ret�ngulo sem o preenchimento
    		Core.rectangle(mat, new Point(this.primeiroPonto.getX(), this.primeiroPonto.getY()), new Point(this.ultimoPonto.getX(), this.ultimoPonto.getY()), scalar);
        }
    }

    @Override
    public boolean contem(Point2D ponto) {

    	Rectangle2D.Double retangulo = new Rectangle2D.Double();
    	
    	retangulo.setFrameFromDiagonal(primeiroPonto, ultimoPonto);
        return retangulo.contains(ponto);
    }
    
    @Override
    public Point2D.Double[] obterPontos() {

        return new Point2D.Double[]{this.primeiroPonto, this.ultimoPonto};
    }
}