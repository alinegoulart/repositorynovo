package modelo;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.opencv.core.Mat;

/**
 * Esta classe armazena os dados de uma determinada figura
 * @author Raiane
 *
 */
public abstract class Figura2D {

	//nome da figura
    private String nome;
    //cor da figura
    private Color cor;
    //se a figura est� preenchida ou n�o 
    private boolean preenchido;
    //se a figura est� selecionada ou n�o
    private boolean selecionado;
    
    /**
     * Inicializa a figura
     */
    protected Figura2D() {

        this("", Color.BLACK, false, false);
    }

    /**
     * Inicializa a figura com algumas informa��es
     * @param nome nome da figura
     * @param cor cor da figura
     * @param preenchido se a figura est� preenchida ou n�o
     * @param selecionado se a figura est� selecionada ou n�o
     */
    public Figura2D(String nome, Color cor, boolean preenchido, boolean selecionado) {
		
    	//copia o conte�do das vari�veis para os atributos 
    	super();
		this.nome = nome;
		this.cor = cor;
		this.preenchido = preenchido;
		this.selecionado = selecionado;
	}

    /**
     * Retorna o nome da figura
     * @return nome da figura
     */
    public String getNome() {
		return nome;
	}

    /**
     * Atualiza o nome da figura
     * @param nome nome da figura
     */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna a cor da figura
	 * @return cor da figura
	 */
	public Color getCor() {
		return cor;
	}

	/**
	 * Atualiza a cor da figura
	 * @param cor cor da figura
	 */
	public void setCor(Color cor) {
		this.cor = cor;
	}

	/**
	 * Retorna se a figura est� preenchida ou n�o
	 * @return true se a figura est� preenchida ou false caso n�o esteja
	 */
	public boolean isPreenchido() {
		return preenchido;
	}

	/**
	 * Atualiza o valor de preenchimento da figura
	 * @param preenchido valor de preechimento
	 */
	public void setPreenchido(boolean preenchido) {
		this.preenchido = preenchido;
	}

	/**
	 * Retorna se a figura est� selecionada ou n�o
	 * @return true se a figura est� selecionada ou false caso n�o esteja
	 */
	public boolean isSelecionado() {
		return selecionado;
	}

	/**
	 * Atualiza o valor de sele��o da figura
	 * @param selecionado valor de sele��o da figura
	 */
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	/**
	 * Checa se esta figura � equivalente a outra figura passada como par�metro
	 */
	public boolean equals(Object o) {

		//converte o objeto para figura
        Figura2D outro = (Figura2D)o;

        //compara se as figuras s�o iguais pelo nome
        return this.nome.equals(outro.nome);
    }

	/**
	 * Desenhar os pontos de uma figura
	 * @param mat local onde vai ser desenhado os pontos
	 */
    public abstract void desenharPontos(Mat mat);

    /**
     * Retorna um vetor com os pontos de da figura
     * @return vetor com os pontos da figura
     */
    public abstract Point2D.Double[] obterPontos();

    /**
     * Checa se um ponto est� dentro de uma figura
     * @param ponto ponto a ser checado
     * @return retorna true se o ponto estiver ou false caso n�o esteja
     */
    public abstract boolean contem(Point2D ponto);

    /**
     * Desenhar a figura
     * @param mat local onde vai ser desenhado a figura
     */
    public abstract void desenharFigura(Mat mat);
}