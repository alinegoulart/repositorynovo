package modelo;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.opencv.core.Mat;

import utilidades.MathUtility;

public class Texto2D extends Figura2D {

    private String texto;
    private Font fonte;
    private BufferedImage imagemTexto;
    private Rectangle2D.Double textoBox;
    private Point2D.Double posicaoTexto;
    private Point2D.Double ultimaPosicaoTexto;
    private float posicaoX;
    private float posicaoY;

    public Texto2D() {

        this(null, null);
    }

    public Texto2D(String texto, Font fonte) {

        this.texto = texto;
        this.fonte = fonte;
        this.imagemTexto = null;
        this.posicaoTexto = new Point2D.Double(100.0, 100.0);
        this.ultimaPosicaoTexto = new Point2D.Double();
    }

    public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Font getFonte() {
		return fonte;
	}

	public void setFonte(Font fonte) {
		this.fonte = fonte;
	}

	public BufferedImage getImagemTexto() {
		return imagemTexto;
	}

	public void setImagemTexto(BufferedImage imagemTexto) {
		this.imagemTexto = imagemTexto;
	}

	public Rectangle2D.Double getTextoBox() {
		return textoBox;
	}

	public void setTextoBox(Rectangle2D.Double textoBox) {
		this.textoBox = textoBox;
	}

	public Point2D.Double getPosicaoTexto() {
		return posicaoTexto;
	}

	public void setPosicaoTexto(Point2D.Double posicaoTexto) {
		this.posicaoTexto = posicaoTexto;
	}

	public Point2D.Double getUltimaPosicaoTexto() {
		return ultimaPosicaoTexto;
	}

	public void setUltimaPosicaoTexto(Point2D.Double ultimaPosicaoTexto) {
		this.ultimaPosicaoTexto = ultimaPosicaoTexto;
	}

	public float getPosicaoX() {
		return posicaoX;
	}

	public void setPosicaoX(float posicaoX) {
		this.posicaoX = posicaoX;
	}

	public float getPosicaoY() {
		return posicaoY;
	}

	public void setPosicaoY(float posicaoY) {
		this.posicaoY = posicaoY;
	}

	public void desenharPontos(Mat mat) {

        //
    }

    public void criarTexto() {

        int largura = MathUtility.getStringWidth(fonte, texto) + 10;
        int altura = MathUtility.getStringHeight(fonte, texto) + 10;
        
        this.imagemTexto = new BufferedImage(largura, altura, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = this.imagemTexto.createGraphics();
        TextLayout layout = null;
        FontRenderContext frc = null;
        Point2D.Float posicaoTexto2 = null;
        
        g2d.setFont(this.fonte);
        g2d.setColor(this.getCor());
        frc = g2d.getFontRenderContext();

        layout = new TextLayout(this.texto, this.fonte, frc);
        posicaoX = 10.0f;
        posicaoY = 10.0f;
        posicaoTexto2 = MathUtility.getTextPosition(layout, posicaoX, posicaoY);
        layout.draw(g2d, (float)posicaoTexto2.getX(), (float)posicaoTexto2.getY());

        this.ultimaPosicaoTexto = new Point2D.Double(posicaoTexto2.getX(), posicaoTexto2.getY());
        this.ultimaPosicaoTexto.setLocation(this.posicaoTexto.getX() + imagemTexto.getWidth(), this.posicaoTexto.getY() + imagemTexto.getHeight());
        this.posicaoX = 0.0f;
        this.posicaoY = 0.0f;
    }

    public void desenharFigura(Mat mat) {

        /*if(this.imagemTexto != null) {

            this.textBox = new Rectangle2D.Double();
            this.textBox.setFrameFromDiagonal(posicaoTexto, ultimaPosicaoTexto);
            g2d.setColor(Color.BLACK);

            if(this.isShapeSelected()) {
            
                g2d.draw(this.textBox);
            }
            
            g2d.drawImage(imagemTexto, (int)posicaoTexto.getX(), (int)posicaoTexto.getY(), null);
        }*/
    }

    public Point2D.Double[] obterPontos() {

        return new Point2D.Double[]{this.posicaoTexto, this.ultimaPosicaoTexto};
    }

    public boolean contem(Point2D pt) {

        return this.textoBox.contains(pt);
    }
}