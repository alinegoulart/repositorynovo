package modelo;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public class Circulo2D extends Figura2D {

    private Point2D.Double primeiroPonto;
    private Point2D.Double ultimoPonto;
    
    public Circulo2D() {

        this(new Point2D.Double(0.0, 0.0), new Point2D.Double(0.0, 0.0));
    }

    public Circulo2D(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }

    public void setLocalizacao(Point2D.Double primeiroPonto, Point2D.Double ultimoPonto) {

        this.primeiroPonto = primeiroPonto;
        this.ultimoPonto = ultimoPonto;
    }

    @Override
    public void desenharPontos(Mat mat) {

        int tamanho = 10;
        
        if(this.isSelecionado()) {

            Scalar cor = new Scalar(0, 0, 0);

            double x = primeiroPonto.getX() - tamanho / 2;
            double y = primeiroPonto.getY() - tamanho / 2;
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), cor, -1);
            
            x = ultimoPonto.getX() - tamanho / 2;
            y = ultimoPonto.getY() - tamanho / 2;
            Core.rectangle(mat, new Point(x, y), new Point(x + tamanho, y + tamanho), cor, -1);
        }
    }

    @Override
    public void desenharFigura(Mat mat) {

        if(this.isPreenchido()) {

            //g2d.fill(circle);
        }
        else {

            //g2d.draw(circle);
        }
    }

    @Override
    public boolean contem(Point2D ponto) {

        return new Ellipse2D.Double(this.primeiroPonto.getY(), this.primeiroPonto.getY(), ultimoPonto.getX(), ultimoPonto.getY()).contains(ponto);
    }

    @Override
    public Point2D.Double[] obterPontos() {

        return new Point2D.Double[]{this.primeiroPonto, this.ultimoPonto};
    }
}