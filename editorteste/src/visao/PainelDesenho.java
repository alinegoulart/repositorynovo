package visao;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import modelo.Figura2D;
import modelo.Imagem2D;
import modelo.Paint;
import modelo.Poligono2D;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public class PainelDesenho extends JPanel {

	protected int posicaoX;
    protected int posicaoY;
    protected int largura;
    protected int altura;
    protected Graphics2D g2;
    protected BufferedImage imagem;
    protected BufferedImage imagemCapturada;
    protected boolean necessitaRedesenhar;
    public static final int OPERACAO_SELECIONAR_OBJETO = 1;
    public static final int OPERACAO_DESENHAR_LINHA = 2;
    public static final int OPERACAO_DESENHAR_CIRCULO = 3;
    public static final int OPERACAO_DESENHAR_RETANGULO = 4;
    public static final int OPERACAO_DESENHAR_POLIGONO = 5;
    private Paint paint;
    private Figura2D figuraAtual;
    private Imagem2D imagemAtual;
    private List<Line2D.Double> listaLinhas;
    private Mat matImagem;
    private int operacaoAtual;
    private boolean primeiroDesenho;
    private boolean poligonoCompleto;
    private boolean pontoSelecionado;

    public PainelDesenho(Paint paint) {

        this.paint = paint;
        this.necessitaRedesenhar = true;
        this.listaLinhas = new ArrayList<Line2D.Double>();
        this.operacaoAtual = OPERACAO_SELECIONAR_OBJETO;
        this.primeiroDesenho = true;
        this.setBackground(Color.WHITE);
    }

    public int getPosicaoX() {
		return posicaoX;
	}

    public void setPosicaoX(int posicaoX) {
		this.posicaoX = posicaoX;
	}

    public int getPosicaoY() {
		return posicaoY;
	}

    public void setPosicaoY(int posicaoY) {
		this.posicaoY = posicaoY;
	}

    public boolean isNecessitaRedesenhar() {
		return necessitaRedesenhar;
	}

    public void setNecessitaRedesenhar(boolean necessitaRedesenhar) {
		this.necessitaRedesenhar = necessitaRedesenhar;
	}

    public Figura2D getFiguraAtual() {
		return figuraAtual;
	}

    public void setFiguraAtual(Figura2D figuraAtual) {
		this.figuraAtual = figuraAtual;
	}

    public int getOperacaoAtual() {
		return operacaoAtual;
	}
    
    public void setOperacaoAtual(int operacaoAtual) {
		this.operacaoAtual = operacaoAtual;
	}
    
    public boolean isPrimeiroDesenho() {
		return primeiroDesenho;
	}

	public void setPrimeiroDesenho(boolean primeiroDesenho) {
		this.primeiroDesenho = primeiroDesenho;
	}

	public boolean isPoligonoCompleto() {
		return poligonoCompleto;
	}

	public void setPoligonoCompleto(boolean poligonoCompleto) {
		this.poligonoCompleto = poligonoCompleto;
	}

	public boolean isPontoSelecionado() {
		return pontoSelecionado;
	}

	public void setPontoSelecionado(boolean pontoSelecionado) {
		this.pontoSelecionado = pontoSelecionado;
	}
	
	public Imagem2D getImagemAtual() {
		return imagemAtual;
	}

	public void setImagemAtual(Imagem2D imagemAtual) {
		this.imagemAtual = imagemAtual;
	}
	
	public BufferedImage getImagem() {
		return imagem;
	}
	
	public void setImagemCapturada(BufferedImage imagemCapturada) {
		this.imagemCapturada = imagemCapturada;
	}

	public int getTotalLinhas() {
		
		return this.listaLinhas.size();
	}
	
	public Line2D.Double obterLinha(int indice) {
		
		return this.listaLinhas.get(indice);
	}
	
	public void limparListaLinhas() {
		
		this.listaLinhas.clear();
	}
	
	public static BufferedImage matToBufferedImage(Mat matrix) {  
	     
		int cols = matrix.cols();  
	    int rows = matrix.rows();  
	    int elemSize = (int)matrix.elemSize();  
	    byte[] data = new byte[cols * rows * elemSize];  
	    int type;  
	     
	    matrix.get(0, 0, data);  
	     
	    switch (matrix.channels()) {  
	       
	    	case 1:  
	    		type = BufferedImage.TYPE_BYTE_GRAY;  
	    		break;  
	       case 3:  
	    	   type = BufferedImage.TYPE_3BYTE_BGR;  
	    	   // bgr to rgb  
	    	   
	    	   byte b;  
	         
	    	   for(int i=0; i<data.length; i=i+3) {  
	    		   b = data[i];  
	    		   data[i] = data[i+2];  
	    		   data[i+2] = b;  
	    	   }  
	    	   break;  
	       default:  
	    	   return null;  
	     }  
	     
	    BufferedImage image = new BufferedImage(cols, rows, type);  
	    image.getRaster().setDataElements(0, 0, cols, rows, data);  
	    return image;  
	}  
	
    public void paint(Graphics g) {

        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
            
        this.matImagem = new Mat(1360, 600, CvType.CV_8UC3);
        this.matImagem.setTo(new Scalar(255, 255, 255));
        
        this.desenharFiguras(g2d);
        this.desenharImagens(g2d);
        g2d.drawImage(matToBufferedImage(this.matImagem), 0, 0, null);
        this.desenharFiguras(g2d);
        
        if(imagemCapturada != null) {
        
        	g2d.drawImage(imagemCapturada, 0, 0, null);
        }
    }

    public void desenharImagens(Graphics2D g2d) {

        int totalImagens = this.paint.totalImagens();

        for(int i = 0; i < totalImagens; i++) {

            Imagem2D imagem = this.paint.obterImagem(i);

            if(!imagem.equals(this.imagemAtual)) {
                
                imagem.desenharImagem(g2d);
            }
        }

        if(this.imagemAtual != null) {

            this.imagemAtual.desenharImagem(g2d);

            if( (this.operacaoAtual == OPERACAO_SELECIONAR_OBJETO) || (this.pontoSelecionado) ) {

                this.imagemAtual.desenharPontos(g2d);
            }
        }
    }

    public void desenharLinhas(Graphics2D g2d) {

        Iterator<Line2D.Double> it = this.listaLinhas.iterator();

        while(it.hasNext()) {

            Line2D.Double linha = it.next();

            g2d.setColor(Color.BLACK);
            g2d.draw(linha);
        }
    }

    public void desenharFiguras(Graphics2D g2d) {

        int totalFiguras = this.paint.totalFiguras();

        for(int i = 0; i < totalFiguras; i++) {

            Figura2D figura = this.paint.obterFigura(i);

            if(figura != figuraAtual) {
            
                figura.desenharFigura(this.matImagem);
            }
        }

        if(this.figuraAtual != null) {

            if(this.operacaoAtual == OPERACAO_DESENHAR_POLIGONO) {

                if(!this.poligonoCompleto) {

                    this.desenharLinhas(g2d);
                }
            }
            else {

                this.figuraAtual.desenharFigura(this.matImagem);
            }

            if(this.operacaoAtual == OPERACAO_SELECIONAR_OBJETO) {

                this.figuraAtual.desenharPontos(this.matImagem);
            }
        }
    }

    public void limparProcessoDesenho() {

        this.poligonoCompleto = false;
        this.listaLinhas.clear();
        this.figuraAtual = null;
        this.imagemAtual = null;
    }

    public void adicionarLinha(Line2D.Double linha) {
        
        this.listaLinhas.add(linha);
    }

    public void construirPoligono() {

        Line2D.Double primeiraLinha = this.listaLinhas.get(0);
        
        ((Poligono2D)this.figuraAtual).adicionarPonto(new Point2D.Double(primeiraLinha.getX1(), primeiraLinha.getY1()));
        ((Poligono2D)this.figuraAtual).adicionarPonto(new Point2D.Double(primeiraLinha.getX2(), primeiraLinha.getY2()));

        for(int i = 1; i < this.listaLinhas.size(); i++) {

            Line2D.Double linha = this.listaLinhas.get(i);

            ((Poligono2D)this.figuraAtual).adicionarPonto(new Point2D.Double(linha.getX1(), linha.getY1()));
            //((Polygon2D)this.figuraAtual).addPoint(new Point2D.Double(line.getX2(), line.getY2()));
        }
    }
}