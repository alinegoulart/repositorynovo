package visao;

import java.awt.GraphicsEnvironment;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class FrmAdicionarTexto extends javax.swing.JFrame {

    /** Creates new form FrmCreateLegend */
    public FrmAdicionarTexto() {

        initComponents();
        this.loadFontsIntoComboBox();
    }

    public JButton getBtnCancelar() {
        return btnCancelar;
    }

    public JButton getBtnAdicionarTexto() {
        return btnAdicionarTexto;
    }

    public JComboBox getCmbFontSelect() {
        return cmbFontSelect;
    }

    public JCheckBox getJcbBold() {
        return jcbBold;
    }

    public JCheckBox getJcbItalic() {
        return jcbItalic;
    }

    public JComboBox getLblSizeSelect() {
        return cmbFontSizeSelect;
    }

    public JComboBox getCmbFontSizeSelect() {
        return cmbFontSizeSelect;
    }

    public JTextField getTxtText() {
        return txtText;
    }

    public void loadFontsIntoComboBox() {

        String fontList[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

        this.cmbFontSelect.removeAllItems();

        for(int i = 0; i < fontList.length; i++) {

            this.cmbFontSelect.addItem(fontList[i]);
        }
    }

    public void inicializarFrmAdicionarTexto() {
        
        this.txtText.setText("");
    }

    public void abrirFrmAdicionarTexto() {

        this.inicializarFrmAdicionarTexto();
        this.setVisible(true);
    }

    public void fecharFrmAdicionarTexto() {

        this.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelText = new javax.swing.JPanel();
        lblText = new javax.swing.JLabel();
        txtText = new javax.swing.JTextField();
        lblFont = new javax.swing.JLabel();
        cmbFontSelect = new javax.swing.JComboBox();
        lblSize = new javax.swing.JLabel();
        cmbFontSizeSelect = new javax.swing.JComboBox();
        jcbItalic = new javax.swing.JCheckBox();
        jcbBold = new javax.swing.JCheckBox();
        btnAdicionarTexto = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setTitle("Adicionar Texto");
        setResizable(false);

        panelText.setBorder(javax.swing.BorderFactory.createTitledBorder("Parâmetros do Texto"));

        lblText.setText("Conteúdo do texto");

        lblFont.setText("Fonte:");

        cmbFontSelect.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        lblSize.setText("Tamanho:");

        cmbFontSizeSelect.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10", "12", "14", "16", "18", "20", "22", "24", "26", "28", "32", "36", "42", "50", "64", "72", "96" }));

        jcbItalic.setText("Itálico");

        jcbBold.setText("Negrito");

        btnAdicionarTexto.setText("Adicionar Texto");

        btnCancelar.setText("Cancelarar");

        javax.swing.GroupLayout panelTextLayout = new javax.swing.GroupLayout(panelText);
        panelText.setLayout(panelTextLayout);
        panelTextLayout.setHorizontalGroup(
            panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTextLayout.createSequentialGroup()
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelTextLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblText)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                        .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelTextLayout.createSequentialGroup()
                                .addComponent(cmbFontSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblSize)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbFontSizeSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtText, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelTextLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelTextLayout.createSequentialGroup()
                                .addGap(446, 446, 446)
                                .addComponent(jcbItalic)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jcbBold))
                            .addGroup(panelTextLayout.createSequentialGroup()
                                .addComponent(lblFont)
                                .addGap(296, 296, 296))))
                    .addGroup(panelTextLayout.createSequentialGroup()
                        .addGap(223, 223, 223)
                        .addComponent(btnAdicionarTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar)))
                .addContainerGap())
        );
        panelTextLayout.setVerticalGroup(
            panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTextLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblText)
                    .addComponent(txtText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFont)
                    .addComponent(jcbItalic)
                    .addComponent(jcbBold)
                    .addComponent(cmbFontSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSize)
                    .addComponent(cmbFontSizeSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdicionarTexto)
                    .addComponent(btnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarTexto;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox cmbFontSelect;
    private javax.swing.JComboBox cmbFontSizeSelect;
    private javax.swing.JCheckBox jcbBold;
    private javax.swing.JCheckBox jcbItalic;
    private javax.swing.JLabel lblFont;
    private javax.swing.JLabel lblSize;
    private javax.swing.JLabel lblText;
    private javax.swing.JPanel panelText;
    private javax.swing.JTextField txtText;
    // End of variables declaration//GEN-END:variables

}
