package visao;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import modelo.Paint;

import org.opencv.core.Core;

import controle.ControlePaint;

/**
 * Classe que cria a interface gráfica principal do aplicativo
 * @author Raiane
 *
 */
public class FrmPaint extends JFrame {

	//armazena os dados do aplicativo
	private Paint paint;
	//armazena a classe de controle
    private ControlePaint controlePaint;
    //armazena o formulário de adicionar texto
    private FrmAdicionarTexto frmAdicionarTexto;
    //armazea o formulário de exportar Imagemm
    private FrmExportarImagem frmExportarImagem;
    //armazena o painel de desenho
    private PainelDesenho painelDesenho;
    //armazena os componentes do formulário (botões, menus, textos...) 
    private JToggleButton btnAdicionarCirculo;
    private JButton btnAdicionarImagem;
    private JToggleButton btnAdicionarLinha;
    private JToggleButton btnAdicionarPoligono;
    private JToggleButton btnAdicionarRetangulo;
    private JButton btnAdicionarTexto;
    private JButton btnCorAzul;
    private JButton btnFechar;
    private JButton btnSelecionarCor;
    private JButton btnCorVerde;
    private JButton btnCorLaranja;
    private JButton btnImprimir;
    private JButton btnCorVermelho;
    private JButton btnRemoverImagem;
    private JButton btnRemoverFigura;
    private JButton btnSaveImagem;
    private JButton btnCapturarImagem;
    private JToggleButton btnSelectCursor;
    private JButton btnCorAmarelo;
    private JMenuBar jMenuBar1;
    private JMenu jMenuFile;
    private JPopupMenu.Separator jSeparator15;
    private JToolBar.Separator jSeparator6;
    private JToolBar.Separator jSeparator7;
    private JPopupMenu.Separator jSeparator8;
    private JToolBar.Separator jSeparator9;
    private JToolBar jToolBar1;
    private JCheckBox jcbFilled;
    private JLabel lblCor;
    private JLabel lblCoordenadas;
    private JLabel lblCorSelecionada;
    private JMenuItem menuItemAdicionarCirculo;
    private JMenuItem menuItemAdicionarImagem;
    private JMenuItem menuItemAdicionarLinha;
    private JMenuItem menuItemAdicionarPoligono;
    private JMenuItem menuItemAdicionarRetangulo;
    private JMenuItem menuItemAdicionarTexto;
    private JMenuItem menuItemAddThemeLegend;
    private JMenuItem menuItemFechar;
    private JMenuItem menuItemCursorSelect;
    private JMenuItem menuItemExportarImagem;
    private JMenuItem menuItemPrint;
    private JMenuItem menuItemRemoverImagem;
    private JMenuItem menuItemRemoverFigura;
    private JMenu menuFerramentas;
    private JMenu menuImagem;
    private JPanel panelCanvasMap;
    
    /**
     * Constrói a interface
     */
    public FrmPaint() {

    	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        this.paint = new Paint();
        this.painelDesenho = new PainelDesenho(this.paint);
        
        //inicializa os componentes
        initComponents();

        this.frmAdicionarTexto = new FrmAdicionarTexto();
        this.frmExportarImagem = new FrmExportarImagem();
        
        this.controlePaint = new ControlePaint(this.paint, this);
        //this.eventsHandlerThemeTree = new EventsHandlerThemeTree(this.view, this);
    }

    /**
     * Métodos get
     * @return
     */
    public FrmAdicionarTexto getFrmAdicionarTexto() {
		return frmAdicionarTexto;
	}

    public FrmExportarImagem getFrmExportarImagem() {
		return frmExportarImagem;
	}

    public PainelDesenho getPainelDesenho() {
		return painelDesenho;
	}

    public JToggleButton getBtnAdicionarCirculo() {
		return btnAdicionarCirculo;
	}

    public JButton getBtnAdicionarImagem() {
		return btnAdicionarImagem;
	}

    public JToggleButton getBtnAdicionarLinha() {
		return btnAdicionarLinha;
	}

    public JToggleButton getBtnAdicionarPoligono() {
		return btnAdicionarPoligono;
	}

    public JToggleButton getBtnAdicionarRetangulo() {
		return btnAdicionarRetangulo;
	}

    public JButton getBtnAdicionarTexto() {
		return btnAdicionarTexto;
	}

    public JButton getBtnCorAzul() {
		return btnCorAzul;
	}

    public JButton getBtnFechar() {
		return btnFechar;
	}

    public JButton getBtnSelecionarCor() {
		return btnSelecionarCor;
	}

    public JButton getBtnCorVerde() {
		return btnCorVerde;
	}

    public JButton getBtnCorLaranja() {
		return btnCorLaranja;
	}

    public JButton getBtnImprimir() {
		return btnImprimir;
	}

    public JButton getBtnCorVermelho() {
		return btnCorVermelho;
	}

    public JButton getBtnRemoverImagem() {
		return btnRemoverImagem;
	}

    public JButton getBtnRemoverFigura() {
		return btnRemoverFigura;
	}

    public JButton getBtnSaveImagem() {
		return btnSaveImagem;
	}

    public JToggleButton getBtnSelectCursor() {
		return btnSelectCursor;
	}

    public JButton getBtnCorAmarelo() {
		return btnCorAmarelo;
	}

    public JCheckBox getJcbFilled() {
		return jcbFilled;
	}

    public JLabel getLblCor() {
		return lblCor;
	}
    
    public JLabel getLblCoordenadas() {
		return lblCoordenadas;
	}

    public JLabel getLblCorSelecionada() {
		return lblCorSelecionada;
	}

    public JMenuItem getMenuItemAdicionarCirculo() {
		return menuItemAdicionarCirculo;
	}

    public JMenuItem getMenuItemAdicionarImagem() {
		return menuItemAdicionarImagem;
	}

    public JMenuItem getMenuItemAdicionarLinha() {
		return menuItemAdicionarLinha;
	}

    public JMenuItem getMenuItemAdicionarPoligono() {
		return menuItemAdicionarPoligono;
	}

    public JMenuItem getMenuItemAdicionarRetangulo() {
		return menuItemAdicionarRetangulo;
	}

    public JMenuItem getMenuItemAdicionarTexto() {
		return menuItemAdicionarTexto;
	}

    public JMenuItem getMenuItemAddThemeLegend() {
		return menuItemAddThemeLegend;
	}

    public JMenuItem getMenuItemFechar() {
		return menuItemFechar;
	}

    public JMenuItem getMenuItemCursorSelect() {
		return menuItemCursorSelect;
	}

    public JMenuItem getMenuItemExportarImagem() {
		return menuItemExportarImagem;
	}

    public JMenuItem getMenuItemPrint() {
		return menuItemPrint;
	}

    public JMenuItem getMenuItemRemoverImagem() {
		return menuItemRemoverImagem;
	}

    public JMenuItem getMenuItemRemoverFigura() {
		return menuItemRemoverFigura;
	}
    
    public JButton getBtnCapturarImagem() {
		return btnCapturarImagem;
	}

	//atualiza o painel de desenho
    public void atualizarCanvas() {

        this.painelDesenho.repaint();
    }

    public void atualizarCanvasNecessitandoRedesenhar() {

        this.painelDesenho.setNecessitaRedesenhar(true);
        this.atualizarCanvas();
    }

    //métodos que ativam ou desativam os componentes da interface
    public void desativarBotoesOperacao() {

        this.btnSelectCursor.setSelected(false);
        this.btnAdicionarCirculo.setSelected(false);
        this.btnAdicionarLinha.setSelected(false);
        this.btnAdicionarRetangulo.setSelected(false);
        this.btnAdicionarPoligono.setSelected(false);
        //this.viewCanvas.clearDrawProcess();
        this.atualizarCanvas();
    }

    public void ativarBtnSelectCursor() {

        this.desativarBotoesOperacao();
        this.btnSelectCursor.setSelected(true);
    }

    public void ativarBtnAdicionarLinha() {

        this.desativarBotoesOperacao();
        this.btnAdicionarLinha.setSelected(true);
    }

    public void ativarBtnAdicionarRetangulo() {

        this.desativarBotoesOperacao();
        this.btnAdicionarRetangulo.setSelected(true);
    }

    public void ativarBtnAdicionarCirculo() {

        this.desativarBotoesOperacao();
        this.btnAdicionarCirculo.setSelected(true);
    }

    public void ativarBtnAdicionarPoligono() {

        this.desativarBotoesOperacao();
        this.btnAdicionarPoligono.setSelected(true);
    }

    public void enableButtonRemoverFigura() {

        this.btnRemoverFigura.setEnabled(true);
    }

    public void disableButtonRemoverFigura() {

        this.btnRemoverFigura.setEnabled(false);
    }

    public void enableMenuItemRemoverFigura() {

        this.menuItemRemoverFigura.setEnabled(true);
    }

    public void disableMenuItemRemoverFigura() {

        this.menuItemRemoverFigura.setEnabled(false);
    }

    public void enableJcbFilled() {

        this.jcbFilled.setEnabled(true);
    }

    public void disableJcbFilled() {

        this.jcbFilled.setEnabled(false);
    }

    public void enableShapeFilled() {

        this.jcbFilled.setSelected(true);
    }

    public void disableShapeFilled() {

        this.jcbFilled.setSelected(false);
    }

    public void enableMenuItemRemoverImagem() {

        this.menuItemRemoverImagem.setEnabled(true);
    }

    public void disableMenuItemRemoverImagem() {

        this.menuItemRemoverImagem.setEnabled(false);
    }

    public void enableButtonRemoverImagem() {

        this.btnRemoverImagem.setEnabled(true);
    }

    public void disableButtonRemoverImagem() {

        this.btnRemoverImagem.setEnabled(false);
    }

    //método que inicializa todos os componentes do formulário
     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new JToolBar();
        btnFechar = new JButton();
        btnSaveImagem = new JButton();
        btnImprimir = new JButton();
        jSeparator7 = new JToolBar.Separator();
        btnAdicionarImagem = new JButton();
        btnRemoverImagem = new JButton();
        btnSelectCursor = new JToggleButton();
        btnAdicionarLinha = new JToggleButton();
        btnAdicionarRetangulo = new JToggleButton();
        btnAdicionarCirculo = new JToggleButton();
        btnAdicionarPoligono = new JToggleButton();
        btnAdicionarTexto = new JButton();
        lblCor = new JLabel();
        lblCorSelecionada = new JLabel();
        jSeparator9 = new JToolBar.Separator();
        btnCorVermelho = new JButton();
        btnCorAmarelo = new JButton();
        btnCorLaranja = new JButton();
        btnCorVerde = new JButton();
        btnCorAzul = new JButton();
        btnSelecionarCor = new JButton();
        jcbFilled = new JCheckBox();
        btnRemoverFigura = new JButton();
        jSeparator6 = new JToolBar.Separator();
        lblCoordenadas = new JLabel();
        panelCanvasMap = painelDesenho;
        jMenuBar1 = new JMenuBar();
        jMenuFile = new JMenu();
        menuItemFechar = new JMenuItem();
        jSeparator8 = new JPopupMenu.Separator();
        menuImagem = new JMenu();
        menuItemAdicionarImagem = new JMenuItem();
        menuItemRemoverImagem = new JMenuItem();
        menuItemAddThemeLegend = new JMenuItem();
        menuItemCursorSelect = new JMenuItem();
        menuFerramentas = new JMenu();
        menuItemAdicionarLinha = new JMenuItem();
        menuItemAdicionarCirculo = new JMenuItem();
        menuItemAdicionarRetangulo = new JMenuItem();
        menuItemAdicionarPoligono = new JMenuItem();
        menuItemAdicionarTexto = new JMenuItem();
        menuItemRemoverFigura = new JMenuItem();
        menuItemExportarImagem = new JMenuItem();
        menuItemPrint = new JMenuItem();
        jSeparator15 = new JPopupMenu.Separator();
        btnCapturarImagem = new JButton("Capturar Imagem");
        setTitle("Paint");

        jToolBar1.setRollover(true);

        btnFechar.setIcon(new ImageIcon(getClass().getResource("/visao/removeproject.png"))); // NOI18N
        btnFechar.setToolTipText("Fechar");
        btnFechar.setFocusable(false);
        btnFechar.setHorizontalTextPosition(SwingConstants.CENTER);
        btnFechar.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnFechar);

        btnSaveImagem.setIcon(new ImageIcon(getClass().getResource("/visao/save.png"))); // NOI18N
        btnSaveImagem.setToolTipText("Exportar Mapa");
        btnSaveImagem.setFocusable(false);
        btnSaveImagem.setHorizontalTextPosition(SwingConstants.CENTER);
        jToolBar1.add(btnSaveImagem);

        btnImprimir.setIcon(new ImageIcon(getClass().getResource("/visao/print.jpg"))); // NOI18N
        btnImprimir.setToolTipText("Imprimir");
        btnImprimir.setFocusable(false);
        btnImprimir.setHorizontalTextPosition(SwingConstants.CENTER);
        btnImprimir.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnImprimir);
        jToolBar1.add(jSeparator7);

        btnAdicionarImagem.setIcon(new ImageIcon(getClass().getResource("/visao/exportImage.png"))); // NOI18N
        btnAdicionarImagem.setToolTipText("Adicionar Imagemm");
        btnAdicionarImagem.setFocusable(false);
        btnAdicionarImagem.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarImagem.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarImagem);

        btnRemoverImagem.setIcon(new ImageIcon(getClass().getResource("/visao/removeimagem.png"))); // NOI18N
        btnRemoverImagem.setToolTipText("Remover Imagemm");
        btnRemoverImagem.setEnabled(false);
        btnRemoverImagem.setFocusable(false);
        btnRemoverImagem.setHorizontalTextPosition(SwingConstants.CENTER);
        btnRemoverImagem.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnRemoverImagem);

        btnSelectCursor.setIcon(new ImageIcon(getClass().getResource("/visao/selectobject.jpg"))); // NOI18N
        btnSelectCursor.setSelected(true);
        btnSelectCursor.setToolTipText("Selecionar Objeto");
        btnSelectCursor.setFocusable(false);
        btnSelectCursor.setHorizontalTextPosition(SwingConstants.CENTER);
        btnSelectCursor.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnSelectCursor);

        btnAdicionarLinha.setIcon(new ImageIcon(getClass().getResource("/visao/line.png"))); // NOI18N
        btnAdicionarLinha.setToolTipText("Adicionar Linha");
        btnAdicionarLinha.setFocusable(false);
        btnAdicionarLinha.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarLinha.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarLinha);

        btnAdicionarRetangulo.setIcon(new ImageIcon(getClass().getResource("/visao/rectangle.png"))); // NOI18N
        btnAdicionarRetangulo.setToolTipText("Adicionar RetÃ¢ngulo");
        btnAdicionarRetangulo.setFocusable(false);
        btnAdicionarRetangulo.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarRetangulo.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarRetangulo);

        btnAdicionarCirculo.setIcon(new ImageIcon(getClass().getResource("/visao/circle.png"))); // NOI18N
        btnAdicionarCirculo.setToolTipText("Adicionar CÃ­rculo");
        btnAdicionarCirculo.setFocusable(false);
        btnAdicionarCirculo.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarCirculo.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarCirculo);

        btnAdicionarPoligono.setIcon(new ImageIcon(getClass().getResource("/visao/polygon.png"))); // NOI18N
        btnAdicionarPoligono.setToolTipText("Adicionar PolÃ­gono");
        btnAdicionarPoligono.setFocusable(false);
        btnAdicionarPoligono.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarPoligono.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarPoligono);

        btnAdicionarTexto.setIcon(new ImageIcon(getClass().getResource("/visao/text.png"))); // NOI18N
        btnAdicionarTexto.setToolTipText("Adicionar Texto");
        btnAdicionarTexto.setFocusable(false);
        btnAdicionarTexto.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdicionarTexto.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnAdicionarTexto);

        lblCor.setText("Cor selecionada:");
        jToolBar1.add(lblCor);

        lblCorSelecionada.setBackground(new java.awt.Color(0, 0, 0));
        lblCorSelecionada.setText("123");
        lblCorSelecionada.setOpaque(true);
        jToolBar1.add(lblCorSelecionada);
        jToolBar1.add(jSeparator9);

        btnCorVermelho.setBackground(new java.awt.Color(255, 0, 51));
        btnCorVermelho.setForeground(new java.awt.Color(255, 0, 51));
        btnCorVermelho.setText("Cor1");
        btnCorVermelho.setToolTipText("");
        btnCorVermelho.setFocusable(false);
        btnCorVermelho.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCorVermelho.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCorVermelho);

        btnCorAmarelo.setBackground(new java.awt.Color(255, 255, 0));
        btnCorAmarelo.setForeground(new java.awt.Color(255, 255, 0));
        btnCorAmarelo.setText("Cor 3");
        btnCorAmarelo.setToolTipText("");
        btnCorAmarelo.setFocusable(false);
        btnCorAmarelo.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCorAmarelo.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCorAmarelo);

        btnCorLaranja.setBackground(new java.awt.Color(255, 153, 0));
        btnCorLaranja.setForeground(new java.awt.Color(255, 153, 0));
        btnCorLaranja.setText("Cor 5");
        btnCorLaranja.setToolTipText("");
        btnCorLaranja.setFocusable(false);
        btnCorLaranja.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCorLaranja.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCorLaranja);

        btnCorVerde.setBackground(new java.awt.Color(102, 204, 0));
        btnCorVerde.setForeground(new java.awt.Color(102, 204, 0));
        btnCorVerde.setText("Cor 2");
        btnCorVerde.setToolTipText("");
        btnCorVerde.setFocusable(false);
        btnCorVerde.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCorVerde.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCorVerde);

        btnCorAzul.setBackground(new java.awt.Color(0, 51, 255));
        btnCorAzul.setForeground(new java.awt.Color(0, 51, 255));
        btnCorAzul.setText("Cor 4");
        btnCorAzul.setToolTipText("");
        btnCorAzul.setFocusable(false);
        btnCorAzul.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCorAzul.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCorAzul);

        btnSelecionarCor.setText("Mais cores...");
        btnSelecionarCor.setToolTipText("Mais cores...");
        btnSelecionarCor.setFocusable(false);
        btnSelecionarCor.setHorizontalTextPosition(SwingConstants.CENTER);
        btnSelecionarCor.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnSelecionarCor);

        jcbFilled.setText("Preenchido");
        jcbFilled.setEnabled(false);
        jcbFilled.setFocusable(false);
        jcbFilled.setHorizontalAlignment(SwingConstants.CENTER);
        jcbFilled.setHorizontalTextPosition(SwingConstants.RIGHT);
        jcbFilled.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(jcbFilled);

        btnRemoverFigura.setIcon(new ImageIcon(getClass().getResource("/visao/removeline.png"))); // NOI18N
        btnRemoverFigura.setToolTipText("Remover Objeto Seleionado");
        btnRemoverFigura.setEnabled(false);
        btnRemoverFigura.setFocusable(false);
        btnRemoverFigura.setHorizontalTextPosition(SwingConstants.CENTER);
        btnRemoverFigura.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnRemoverFigura);
        jToolBar1.add(jSeparator6);
        
        btnCapturarImagem.setFocusable(false);
        btnCapturarImagem.setHorizontalTextPosition(SwingConstants.CENTER);
        btnCapturarImagem.setVerticalTextPosition(SwingConstants.BOTTOM);
        jToolBar1.add(btnCapturarImagem);
        
        jMenuFile.setText("Arquivo");

        menuItemFechar.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.CTRL_MASK));
        menuItemFechar.setIcon(new ImageIcon(getClass().getResource("/visao/removeproject.png"))); // NOI18N
        menuItemFechar.setText("Fechar...");
        jMenuFile.add(menuItemFechar);
        jMenuFile.add(jSeparator8);

        jMenuBar1.add(jMenuFile);

        menuImagem.setText("Vistas");

        menuItemAdicionarImagem.setIcon(new ImageIcon(getClass().getResource("/visao/exportimage.png"))); // NOI18N
        menuItemAdicionarImagem.setText("Adicionar Imagemm...");
        menuImagem.add(menuItemAdicionarImagem);

        menuItemRemoverImagem.setIcon(new ImageIcon(getClass().getResource("/visao/removeimagem.png"))); // NOI18N
        menuItemRemoverImagem.setText("Remover Imagemm...");
        menuItemRemoverImagem.setEnabled(false);
        menuImagem.add(menuItemRemoverImagem);

        jMenuBar1.add(menuImagem);

        menuFerramentas.setText("Ferramentas");

        menuItemAdicionarLinha.setIcon(new ImageIcon(getClass().getResource("/visao/line.png"))); // NOI18N
        menuItemAdicionarLinha.setText("Adicionar Linha");
        menuFerramentas.add(menuItemAdicionarLinha);

        menuItemAdicionarCirculo.setIcon(new ImageIcon(getClass().getResource("/visao/circle.png"))); // NOI18N
        menuItemAdicionarCirculo.setText("Adicionar CÃ­rculo");
        menuFerramentas.add(menuItemAdicionarCirculo);

        menuItemAdicionarRetangulo.setIcon(new ImageIcon(getClass().getResource("/visao/rectangle.png"))); // NOI18N
        menuItemAdicionarRetangulo.setText("Adicionar RetÃ¢ngulo");
        menuFerramentas.add(menuItemAdicionarRetangulo);

        menuItemAdicionarPoligono.setIcon(new ImageIcon(getClass().getResource("/visao/polygon.png"))); // NOI18N
        menuItemAdicionarPoligono.setText("Adicionar PolÃ­gono");
        menuFerramentas.add(menuItemAdicionarPoligono);

        menuItemAdicionarTexto.setIcon(new ImageIcon(getClass().getResource("/visao/text.png"))); // NOI18N
        menuItemAdicionarTexto.setText("Adicionar Texto");
        menuFerramentas.add(menuItemAdicionarTexto);

        menuItemRemoverFigura.setIcon(new ImageIcon(getClass().getResource("/visao/removeline.png"))); // NOI18N
        menuItemRemoverFigura.setText("Remover Objeto Selecionado");
        menuItemRemoverFigura.setEnabled(false);
        menuFerramentas.add(menuItemRemoverFigura);

        menuItemExportarImagem.setIcon(new ImageIcon(getClass().getResource("/visao/exportimage.png"))); // NOI18N
        menuItemExportarImagem.setText("Exportar para Imagemm...");
        menuFerramentas.add(menuItemExportarImagem);

        menuItemPrint.setIcon(new ImageIcon(getClass().getResource("/visao/print.jpg"))); // NOI18N
        menuItemPrint.setText("Imprimir...");
        menuFerramentas.add(menuItemPrint);
        menuFerramentas.add(jSeparator15);

        jMenuBar1.add(menuFerramentas);

        setJMenuBar(jMenuBar1);

        this.getContentPane().setLayout(null);
        this.jToolBar1.setBounds(0, 0, 1280, 25);
        this.panelCanvasMap.setBounds(10, 40, 1280, 600);
        this.getContentPane().add(this.jToolBar1);
        this.getContentPane().add(this.panelCanvasMap);
        
        setSize(1300, 700);
        setResizable(false);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Método principal 
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {

        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        FrmPaint frmShowView = new FrmPaint();

        frmShowView.setExtendedState(frmShowView.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        frmShowView.setVisible(true);
    }
}