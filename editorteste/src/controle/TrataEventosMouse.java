package controle;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import modelo.Circulo2D;
import modelo.Figura2D;
import modelo.Imagem2D;
import modelo.Linha2D;
import modelo.Poligono2D;
import modelo.Retangulo2D;
import visao.FrmPaint;
import visao.PainelDesenho;

public class TrataEventosMouse extends MouseAdapter {

    private Point primeiroPonto;
    private ControlePaint controlePaint;
    private PainelDesenho painelDesenho;
    private FrmPaint frmPaint;
    private Point2D.Double pontos[];
    private boolean figuraSelecionada;
    private boolean imagemSelecionada;
    private int indicePonto;
    
    public TrataEventosMouse(ControlePaint controlePaint, FrmPaint frmPaint) {

        this.controlePaint = controlePaint;
        this.frmPaint = frmPaint;
        this.painelDesenho = frmPaint.getPainelDesenho();
        this.primeiroPonto = null;
        this.pontos = new Point2D.Double[]{};
        this.indicePonto = -1;
        this.figuraSelecionada = false;
    }

    public void operacaoSelecionarPontos() {

        int tamanho = 10;

        this.indicePonto = -1;

        for(int i = 0; i < this.pontos.length; i++) {

            double x = pontos[i].getX() - tamanho / 2;
            double y = pontos[i].getY() - tamanho / 2;

            if(new java.awt.geom.Rectangle2D.Double(x, y, 10, 10).contains(primeiroPonto)) {

                this.indicePonto = i;
                this.painelDesenho.setPontoSelecionado(true);

                return;
            }
        }
    }

    public void operacaoSelecionarFigura() {

        Figura2D figuraAtual = this.controlePaint.contemFigura(primeiroPonto);

        this.operacaoSelecionarPontos();

        if(this.indicePonto != -1) {

            return;
        }
        
        if(figuraAtual != null) {

            this.figuraSelecionada = true;
            this.imagemSelecionada = false;
            this.pontos = figuraAtual.obterPontos();
            figuraAtual.setSelecionado(true);
            this.painelDesenho.setImagemAtual(null);
            this.painelDesenho.setFiguraAtual(figuraAtual);
            this.frmPaint.atualizarCanvas();
            this.frmPaint.enableJcbFilled();
            this.frmPaint.enableButtonRemoverFigura();;
            this.frmPaint.enableMenuItemRemoverFigura();;

            if(figuraAtual.isPreenchido()) {

                this.frmPaint.enableShapeFilled();
            }
            else {

                this.frmPaint.disableShapeFilled();
            }

            this.frmPaint.getLblCorSelecionada().setBackground(figuraAtual.getCor());
            this.frmPaint.getLblCorSelecionada().setForeground(figuraAtual.getCor());

            return;
        }
        else {

            this.controlePaint.removerTodasSelecoes();
            this.painelDesenho.setFiguraAtual(null);
            this.pontos = new Point2D.Double[]{};
            this.indicePonto = -1;
            this.frmPaint.disableJcbFilled();
            this.frmPaint.disableButtonRemoverFigura();
            this.frmPaint.disableMenuItemRemoverImagem();
            this.frmPaint.atualizarCanvas();
        }
    }

    public void operacaoSelecionarImagem() {

        Imagem2D imagemAtual = this.controlePaint.contemImagem(primeiroPonto);

        this.operacaoSelecionarPontos();

        if(this.indicePonto != -1) {

            return;
        }

        if(imagemAtual != null) {

            this.imagemSelecionada = true;
            this.figuraSelecionada = false;
            this.pontos = imagemAtual.obterPontos();
            imagemAtual.setImageSelecionada(true);
            this.painelDesenho.setFiguraAtual(null);
            this.painelDesenho.setImagemAtual(imagemAtual);
            this.frmPaint.enableButtonRemoverImagem();
            this.frmPaint.enableMenuItemRemoverImagem();

            for(int i = 0; i < this.pontos.length; i++) {

                if(new java.awt.geom.Rectangle2D.Double(pontos[i].getX(), pontos[i].getY(), 10, 10).contains(primeiroPonto)) {

                    this.indicePonto = i;

                    break;
                }
            }

            this.frmPaint.atualizarCanvasNecessitandoRedesenhar();

            return;
        }
        else {

            this.controlePaint.removerTodasSelecoes();
            this.painelDesenho.setImagemAtual(null);
            this.pontos = new Point2D.Double[]{};
            this.indicePonto = -1;
            this.frmPaint.disableButtonRemoverImagem();
            this.frmPaint.disableMenuItemRemoverImagem();
            this.frmPaint.atualizarCanvasNecessitandoRedesenhar();
        }
    }

    public void operacaoSelecionarObjeto() {

        this.operacaoSelecionarFigura();
        
        if(this.painelDesenho.getFiguraAtual() != null) {
            
            return;
        }
        
        this.operacaoSelecionarImagem();
    }

    public void operacaoIniciarDesenhoLinha() {

        Point2D.Double p1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double p2 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());

        this.painelDesenho.setFiguraAtual(new Linha2D(p1, p2));
    }

    public void operacaoIniciarDesenhoRetangulo() {

        Point2D.Double p1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double p2 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());

        this.painelDesenho.setFiguraAtual(new Retangulo2D(p1, p2));
    }

    public void operacaoIniciarDesenhoCirculo() {

        Point2D.Double p1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double p2 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());

        this.painelDesenho.setFiguraAtual(new Circulo2D(p1, p2));
    }

    public void operacaoIniciarOuPararDesenhoPoligono(int buttonMouseType) {

        java.awt.geom.Line2D.Double linha = new java.awt.geom.Line2D.Double(primeiroPonto, primeiroPonto);

        if(this.painelDesenho.getTotalLinhas() == 0) {

            this.painelDesenho.setPoligonoCompleto(false);
            this.painelDesenho.setFiguraAtual(new Poligono2D());
        }

        if( (!painelDesenho.isPoligonoCompleto()) && (buttonMouseType == MouseEvent.BUTTON1) ) {

            linha = new java.awt.geom.Line2D.Double(primeiroPonto, primeiroPonto);
            this.painelDesenho.adicionarLinha(linha);
        }
        else if( (buttonMouseType == MouseEvent.BUTTON3) && (this.painelDesenho.getTotalLinhas() >= 3) ) {

            java.awt.geom.Line2D.Double primeiraLinha = this.painelDesenho.obterLinha(0);
            linha = new java.awt.geom.Line2D.Double(primeiroPonto, primeiraLinha.getP1());
            this.painelDesenho.adicionarLinha(linha);
            this.painelDesenho.setPoligonoCompleto(true);
            this.painelDesenho.construirPoligono();
            this.painelDesenho.limparListaLinhas();
            this.controlePaint.adicionarPoligono((Poligono2D)this.painelDesenho.getFiguraAtual());
            this.painelDesenho.setFiguraAtual(null);
        }

        this.frmPaint.atualizarCanvas();
    }

    public void operacaoMoverLinha(Point pontoAtual) {

        Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

        ((Linha2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
        this.frmPaint.atualizarCanvas();
    }

    public void operacaoMoverRetangulo(Point pontoAtual) {

        Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

        ((Retangulo2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
        this.frmPaint.atualizarCanvas();
    }

    public void operacaoMoverCirculo(Point pontoAtual) {

        Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
        Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

        ((Circulo2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
        this.frmPaint.atualizarCanvas();
    }

    public void moveLinhaPoligono(Point pontoAtual) {

        int totalLinhas = painelDesenho.getTotalLinhas();

        if( (!this.painelDesenho.isPoligonoCompleto()) && (this.painelDesenho.getTotalLinhas() > 0) ) {

            this.painelDesenho.obterLinha(totalLinhas - 1).setLine(this.primeiroPonto, pontoAtual);
            this.frmPaint.atualizarCanvas();
        }
    }

    public void moverFigura(Point pontoAtual) {

        if(this.figuraSelecionada) {

            if(this.indicePonto == -1) {

                for(int i = 0; i < this.pontos.length; i++) {

                    Point2D.Double pt = pontos[i];
                    double x = pontoAtual.getX() - primeiroPonto.getX();
                    double y = pontoAtual.getY() - primeiroPonto.getY();

                    x += pt.getX();
                    y += pt.getY();

                    pt.setLocation(x, y);
                }

                this.primeiroPonto = pontoAtual;
            }
            else {

                Point pt = pontoAtual;

                this.pontos[this.indicePonto].setLocation(pt.getX(), pt.getY());
            }

            this.frmPaint.atualizarCanvas();
        }
    }

    public void moverImagem(Point pontoAtual) {

        if( (this.imagemSelecionada) && (pontos.length > 0) ) {

            if(this.indicePonto == -1) {

                Imagem2D imagemAtual = (Imagem2D)this.painelDesenho.getImagemAtual();

                for(int i = 0; i < this.pontos.length; i++) {

                    Point2D.Double pt = pontos[i];
                    double x = pontoAtual.getX() - primeiroPonto.getX();
                    double y = pontoAtual.getY() - primeiroPonto.getY();

                    x += pt.getX();
                    y += pt.getY();

                    pt.setLocation(x, y);
                }

                imagemAtual.setPosicaoX((int)this.pontos[0].getX());
                imagemAtual.setPosicaoY((int)this.pontos[0].getY());
                imagemAtual.setLargura((int)(this.pontos[1].getX() - this.pontos[0].getX()));
                imagemAtual.setAltura((int)(this.pontos[1].getY() - this.pontos[0].getY()));
                this.frmPaint.atualizarCanvas();
                this.primeiroPonto = pontoAtual;
            }
            else {

                Point pt = pontoAtual;
                Imagem2D imagemAtual = (Imagem2D)this.painelDesenho.getImagemAtual();
                int largura = (int)(pt.getX() - this.pontos[0].getX());
                int altura = (int)(pt.getY() - this.pontos[0].getY());

                if(largura < 10) {

                    largura = 10;
                }
                if(altura < 10) {

                    altura = 10;
                }

                this.pontos[this.indicePonto].setLocation(pt.getX(), pt.getY());

                imagemAtual.setPosicaoX((int)this.pontos[0].getX());
                imagemAtual.setPosicaoY((int)this.pontos[0].getY());
                imagemAtual.setLargura((int)(this.pontos[1].getX() - this.pontos[0].getX()));
                imagemAtual.setAltura((int)(this.pontos[1].getY() - this.pontos[0].getY()));
                this.frmPaint.atualizarCanvas();
            }
        }
    }

    public void operacaoFinalizarDesenhoLinha(Point pontoAtual) {

        if(!this.primeiroPonto.equals(pontoAtual)) {

            Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
            Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

            ((Linha2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
            this.controlePaint.adicionarLinha((Linha2D)this.painelDesenho.getFiguraAtual());
        }

        this.frmPaint.atualizarCanvas();
    }

    public void operacaoFinalizarDesenhoCirculo(Point pontoAtual) {

        if(!this.primeiroPonto.equals(pontoAtual)) {

                Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
                Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

                ((Circulo2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
                this.controlePaint.adicionarCirculo((Circulo2D)this.painelDesenho.getFiguraAtual());
            }

            this.frmPaint.atualizarCanvasNecessitandoRedesenhar();
    }

    public void operacaoFinalizarDesenhoRetangulo(Point pontoAtual) {

        if(!this.primeiroPonto.equals(pontoAtual)) {

            Point2D.Double pt1 = new Point2D.Double(this.primeiroPonto.getX(), this.primeiroPonto.getY());
            Point2D.Double pt2 = new Point2D.Double(pontoAtual.getX(), pontoAtual.getY());

            ((Retangulo2D)this.painelDesenho.getFiguraAtual()).setLocalizacao(pt1, pt2);
            this.controlePaint.adicionarRetangulo((Retangulo2D)this.painelDesenho.getFiguraAtual());
        }

        this.frmPaint.atualizarCanvas();
    }

    public void operacaoFinalizarMoverImagem() {

        if(this.imagemSelecionada) {

            Imagem2D imagemAtual = (Imagem2D)painelDesenho.getImagemAtual();
            Point2D primeiroPonto2 = new Point2D.Double(imagemAtual.getPosicaoX(), imagemAtual.getPosicaoY());
            Point2D lastPoint = new Point2D.Double(imagemAtual.getPosicaoX() + imagemAtual.getLargura(), imagemAtual.getPosicaoY() + imagemAtual.getAltura());
            /*Imagem2D imagem = new Imagem2D(imagemAtual.getNome(), imagemAtual.getImagem(), 

            georeferencedImage.setImageBehindThemes(imagemAtual.isImageBehindThemes());
            this.controlePaint.removerImagem(imagemAtual);
            this.controlePaint.adicionarImagem2D(nome, caminho);
            
            this.frmPaint.refreshViewCanvasNeedRedraw();*/
        }
    }

    public void mousePressed(MouseEvent e) {

        this.primeiroPonto = e.getPoint();

        switch(this.painelDesenho.getOperacaoAtual()) {

            case PainelDesenho.OPERACAO_SELECIONAR_OBJETO:
                
                this.operacaoSelecionarObjeto();
            break;
            case PainelDesenho.OPERACAO_DESENHAR_LINHA:

                this.operacaoIniciarDesenhoLinha();
            break;
            case PainelDesenho.OPERACAO_DESENHAR_CIRCULO:

                this.operacaoIniciarDesenhoCirculo();
            break;
            case PainelDesenho.OPERACAO_DESENHAR_RETANGULO:

                this.operacaoIniciarDesenhoRetangulo();
            break;
            case PainelDesenho.OPERACAO_DESENHAR_POLIGONO:

                this.operacaoIniciarOuPararDesenhoPoligono(e.getButton());
            break;
        }

        this.controlePaint.atualizarCoordenadas(e.getPoint());
    }

    public void mouseReleased(MouseEvent e) {

        switch(this.painelDesenho.getOperacaoAtual()) {

            case PainelDesenho.OPERACAO_SELECIONAR_OBJETO:

                this.operacaoFinalizarMoverImagem();
            break;
            case PainelDesenho.OPERACAO_DESENHAR_LINHA:

                this.operacaoFinalizarDesenhoLinha(e.getPoint());
            break;
            case PainelDesenho.OPERACAO_DESENHAR_RETANGULO:

                this.operacaoFinalizarDesenhoRetangulo(e.getPoint());
            break;
            case PainelDesenho.OPERACAO_DESENHAR_CIRCULO:

                this.operacaoFinalizarDesenhoCirculo(e.getPoint());
            break;
        }

        this.controlePaint.atualizarCoordenadas(e.getPoint());
    }

    public void mouseDragged(MouseEvent e) {

        switch(this.painelDesenho.getOperacaoAtual()) {

            case PainelDesenho.OPERACAO_SELECIONAR_OBJETO:

                this.moverFigura(e.getPoint());
                this.moverImagem(e.getPoint());
            break;
            case PainelDesenho.OPERACAO_DESENHAR_LINHA:

                this.operacaoMoverLinha(e.getPoint());
            break;
            case PainelDesenho.OPERACAO_DESENHAR_RETANGULO:

                this.operacaoMoverRetangulo(e.getPoint());
            break;
            case PainelDesenho.OPERACAO_DESENHAR_CIRCULO:

                this.operacaoMoverCirculo(e.getPoint());
            break;
        }
    }

    public void mouseMoved(MouseEvent e) {

        switch(this.painelDesenho.getOperacaoAtual()) {

            case PainelDesenho.OPERACAO_DESENHAR_POLIGONO:

                this.moveLinhaPoligono(e.getPoint());
            break;
        }
        
        this.controlePaint.atualizarCoordenadas(e.getPoint());
    }
}