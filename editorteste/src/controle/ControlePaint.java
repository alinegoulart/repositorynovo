package controle;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import modelo.Circulo2D;
import modelo.Figura2D;
import modelo.Imagem2D;
import modelo.Linha2D;
import modelo.Paint;
import modelo.Poligono2D;
import modelo.Retangulo2D;
import modelo.Texto2D;
import visao.FrmPaint;
import visao.PainelDesenho;

/**
 * Classe que realiza a interliga��o entre o modelo e vis�o
 * @author Raiane
 *
 */
public class ControlePaint {

	//armazena os dados da aplicativo
	private Paint paint;
	//armazena o painel de desenho
    private PainelDesenho painelDesenho;
    //armazena as classes que tratam os eventos
	private TrataEventosPaint trataEventosPaint;
	private TrataEventosMouse trataEventosMouse;
	//armazena a interface gr�fica
    private FrmPaint frmPaint;
    
    /*
     * Cria o controle a partir do modelo e a vis�o
     */
    public ControlePaint(Paint paint, FrmPaint frmPaint) {

        super();
        //copia os valores das vari�veis para os atributos
        this.paint = paint;
        this.frmPaint = frmPaint;
        this.painelDesenho = frmPaint.getPainelDesenho();
        this.trataEventosPaint = new TrataEventosPaint(this, frmPaint);
    }
    
    public Paint getPaint() {
		return paint;
	}

    /**
     * Adiciona uma linha no paint
     * @param linha linha a ser adicionada
     */
	public void adicionarLinha(Linha2D linha) {

		//obt�m a cor selecionada
        Color cor = this.frmPaint.getLblCorSelecionada().getBackground();

        //seto a cor da linha
        linha.setCor(cor);
        //grava o nome da figura
        linha.setNome("Linha_" + this.paint.totalFiguras());
        //adiciona a linha no paint
        this.paint.adicionarFigura(linha);
    }

    public void adicionarCirculo(Circulo2D circulo) {

        Color cor = this.frmPaint.getLblCorSelecionada().getBackground();

        circulo.setCor(cor);
        circulo.setNome("Circulo_" + this.paint.totalFiguras());
        this.paint.adicionarFigura(circulo);
    }

    public void adicionarRetangulo(Retangulo2D retangulo) {

        Color cor = this.frmPaint.getLblCorSelecionada().getBackground();

        retangulo.setCor(cor);
        retangulo.setNome("Retangulo_" + this.paint.totalFiguras());
        this.paint.adicionarFigura(retangulo);
    }

    public void adicionarPoligono(Poligono2D poligono) {

        Color cor = this.frmPaint.getLblCorSelecionada().getBackground();

        poligono.setCor(cor);
        poligono.setNome("Poligono_" + this.paint.totalFiguras());
        this.paint.adicionarFigura(poligono);
    }

    public void adicionarTexto(Texto2D texto) {

        texto.setNome("Texto_" + this.paint.totalFiguras());
        this.paint.adicionarFigura(texto);
    }

    /**
     * Remove a figura do paint
     * @param figura figura a ser removida
     */
    public void removerFigura(Figura2D figura) {

    	//remove a figura do paint
        this.paint.removerFigura(figura);
    }

    /**
     * Adicionar uma imagem no aplicativo
     * @param nome nome da imagem
     * @param caminho caminho da imagem
     * @throws IOException
     */
    public void adicionarImagem2D(String nome, String caminho) throws IOException {

    	//carrega a imagem do arquivo
        BufferedImage imagem = ImageIO.read(new File(caminho));
        Imagem2D imagem2D = new Imagem2D(nome, imagem, false, 100, 100, 150, 150);

        //adicionar a imagem no aplicativo
        this.paint.adicionarImagem(imagem2D);
    }
    
    /**
     * Remove uma imagem do paint
     * @param imagem imagem a ser removida
     */
    public void removerImagem(Imagem2D imagem) {

    	//remove a imagem do paint
        this.paint.removerImagem(imagem);
    }

    /**
     * Checa se um ponto est� contido na figura
     * @param ponto ponto a ser procurado
     * @return
     */
    public Figura2D contemFigura(Point2D ponto) {

        int totalFiguras = this.paint.totalFiguras();
        Figura2D figuraAtual = null;
        
        //percorre todas as figuras
        for(int i = 0; i < totalFiguras; i++) {

        	//obt�m a figura atual
            Figura2D figura = this.paint.obterFigura(i);

            //checa se o ponto est� contido, se estiver 
            if(figura.contem(ponto)) {

            	figuraAtual = figura;
                //para a busca
            	break;
            }
        }

        //retorna a figura encontradas
        return figuraAtual;
    }

    public Imagem2D contemImagem(Point2D ponto) {

        int totalImagens = this.paint.totalImagens();
        Imagem2D imagemAtual = null;

        for(int i = 0; i < totalImagens; i++) {

            Imagem2D imagem = this.paint.obterImagem(i);

            if(imagem.contem(ponto)) {

                imagemAtual = imagem;
            }
        }

        return imagemAtual;
    }

    /**
     * Remove todas as figuras selecionadas
     */
    public void removerFigurasSelecionadas() {

    	int totalFiguras = this.paint.totalFiguras();
        
    	//percorre todas as figuras
        for(int i = 0; i < totalFiguras; i++) {

        	//obt�m a figura atual
            Figura2D figura = this.paint.obterFigura(i);
            
            //remove a sele��o da figura
            figura.setSelecionado(false);
        }
    }

    public void removerImagensSelecionadas() {

    	int totalImagens = this.paint.totalImagens();
        
        for(int i = 0; i < totalImagens; i++) {

            Imagem2D imagem = this.paint.obterImagem(i);

            imagem.setImageSelecionada(false);
        }
    }

    public void removerTodasSelecoes() {

        this.removerFigurasSelecionadas();
        this.removerImagensSelecionadas();
    }

    public void atualizarCoordenadas(Point p) {

        this.frmPaint.getLblCoordenadas().setText("x: " + p.x + ", y: " + p.y);
    }
}