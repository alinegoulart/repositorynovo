package controle;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import modelo.Figura2D;
import modelo.Imagem2D;
import modelo.Mat2Image;
import modelo.Texto2D;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;

import visao.FrmAdicionarTexto;
import visao.FrmExportarImagem;
import visao.FrmPaint;
import visao.PainelDesenho;

public class TrataEventosPaint implements ActionListener, ItemListener, ChangeListener {

    private ControlePaint controlePaint;
    private TrataEventosMouse trataEventosMouse;
    private PainelDesenho painelDesenho;
    private FrmPaint frmPaint;
    private FrmAdicionarTexto frmAdicionarTexto;
    private FrmExportarImagem frmExportarImagem;

    public TrataEventosPaint(ControlePaint controlePaint, FrmPaint frmPaint) {

        this.controlePaint = controlePaint;
        this.frmPaint = frmPaint;
        this.frmAdicionarTexto = frmPaint.getFrmAdicionarTexto();
        this.frmExportarImagem = frmPaint.getFrmExportarImagem();
        this.painelDesenho = frmPaint.getPainelDesenho();
        this.trataEventosMouse = new TrataEventosMouse(controlePaint, frmPaint);
        this.configureEventsPainelDesenho();
    }

    public void configureEventsPainelDesenho() {

        this.frmPaint.getBtnSaveImagem().addActionListener(this);
        this.frmPaint.getMenuItemPrint().addActionListener(this);
        this.frmPaint.getMenuItemCursorSelect().addChangeListener(this);
        this.frmPaint.getMenuItemCursorSelect().addActionListener(this);
        this.frmPaint.getMenuItemAddThemeLegend().addActionListener(this);
        this.frmPaint.getMenuItemAdicionarImagem().addActionListener(this);
        this.frmPaint.getMenuItemRemoverImagem().addActionListener(this);
        this.frmPaint.getMenuItemAdicionarLinha().addChangeListener(this);
        this.frmPaint.getMenuItemAdicionarCirculo().addChangeListener(this);
        this.frmPaint.getMenuItemAdicionarRetangulo().addChangeListener(this);
        this.frmPaint.getMenuItemAdicionarPoligono().addChangeListener(this);
        this.frmPaint.getMenuItemAdicionarTexto().addActionListener(this);
        this.frmPaint.getMenuItemExportarImagem().addActionListener(this);
        this.frmPaint.getMenuItemRemoverFigura().addActionListener(this);
        this.frmPaint.getMenuItemFechar().addActionListener(this);
        this.frmPaint.getBtnSelecionarCor().addActionListener(this);
        this.frmPaint.getBtnCorVermelho().addActionListener(this);
        this.frmPaint.getBtnCorVerde().addActionListener(this);
        this.frmPaint.getBtnCorAzul().addActionListener(this);
        this.frmPaint.getBtnCorAmarelo().addActionListener(this);
        this.frmPaint.getBtnCorLaranja().addActionListener(this);
        this.frmPaint.getBtnSaveImagem().addActionListener(this);
        this.frmPaint.getBtnImprimir().addActionListener(this);
        this.frmPaint.getBtnSelectCursor().addActionListener(this);
        this.frmPaint.getBtnAdicionarImagem().addActionListener(this);
        this.frmPaint.getBtnRemoverImagem().addActionListener(this);
        this.frmPaint.getBtnAdicionarLinha().addActionListener(this);
        this.frmPaint.getBtnAdicionarCirculo().addActionListener(this);
        this.frmPaint.getBtnAdicionarRetangulo().addActionListener(this);
        this.frmPaint.getBtnAdicionarPoligono().addActionListener(this);
        this.frmPaint.getBtnAdicionarTexto().addActionListener(this);
        this.frmPaint.getBtnRemoverFigura().addActionListener(this);
        this.frmPaint.getBtnFechar().addActionListener(this);
        this.frmPaint.getBtnCapturarImagem().addActionListener(this);
        this.frmPaint.getJcbFilled().addItemListener(this);
        this.frmAdicionarTexto.getBtnAdicionarTexto().addActionListener(this);
        this.frmAdicionarTexto.getBtnCancelar().addActionListener(this);
        this.frmExportarImagem.getBtnDirectorySelect().addActionListener(this);
        this.frmExportarImagem.getBtnMapExport().addActionListener(this);
        this.frmExportarImagem.getBtnCancel().addActionListener(this);
        this.painelDesenho.addMouseListener(this.trataEventosMouse);
        this.painelDesenho.addMouseMotionListener(this.trataEventosMouse);
    }

    public void ativarOuDesativarAdicionarLinhaOperacao() {

        this.painelDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        if(this.frmPaint.getBtnAdicionarLinha().isSelected()) {

            this.frmPaint.ativarBtnAdicionarLinha();
            this.painelDesenho.setOperacaoAtual(PainelDesenho.OPERACAO_DESENHAR_LINHA);
        }
        else {

            this.painelDesenho.setOperacaoAtual(-1);
            this.frmPaint.desativarBotoesOperacao();
        }
    }

    public void ativarOuDesativarAddCircleOperacao() {

        this.painelDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        if(this.frmPaint.getBtnAdicionarCirculo().isSelected()) {

            this.frmPaint.ativarBtnAdicionarCirculo();
            this.painelDesenho.setOperacaoAtual(PainelDesenho.OPERACAO_DESENHAR_CIRCULO);
        }
        else {

            this.painelDesenho.setOperacaoAtual(-1);
            this.frmPaint.desativarBotoesOperacao();
        }
    }

    public void ativarOuDesativarAdicionarRetanguloOperacao() {

        this.painelDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        if(this.frmPaint.getBtnAdicionarRetangulo().isSelected()) {

            this.frmPaint.ativarBtnAdicionarRetangulo();
            this.painelDesenho.setOperacaoAtual(PainelDesenho.OPERACAO_DESENHAR_RETANGULO);
        }
        else {

            this.painelDesenho.setOperacaoAtual(-1);
            this.frmPaint.desativarBotoesOperacao();
        }
    }

    public void ativarOuDesativarAdicionarPoligonoOperacao() {

        this.painelDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        if(this.frmPaint.getBtnAdicionarPoligono().isSelected()) {

            this.frmPaint.ativarBtnAdicionarPoligono();
            this.painelDesenho.setOperacaoAtual(PainelDesenho.OPERACAO_DESENHAR_POLIGONO);
        }
        else {

            this.painelDesenho.setOperacaoAtual(-1);
            this.frmPaint.desativarBotoesOperacao();
        }
    }

    public void addTextOperacao() {

        String text = this.frmAdicionarTexto.getTxtText().getText();
        String fontName = (String)this.frmAdicionarTexto.getCmbFontSelect().getSelectedItem();
        int fontSize = Integer.parseInt((String)this.frmAdicionarTexto.getCmbFontSizeSelect().getSelectedItem());
        Font font = null;
        int fontStyle = Font.PLAIN;
        Texto2D text2D = null;
        Color color = this.frmPaint.getLblCorSelecionada().getBackground();

        if(this.frmAdicionarTexto.getJcbItalic().isSelected()) {

            fontStyle = Font.ITALIC;
        }
        else if(this.frmAdicionarTexto.getJcbBold().isSelected()) {

            fontStyle = Font.BOLD;
        }

        font = new Font(fontName, fontStyle, fontSize);

        text2D = new Texto2D(text, font);
        text2D.setCor(color);
        
        text2D.criarTexto();
        this.controlePaint.adicionarTexto(text2D);
        //this.frmAdicionarTexto.closeFrmAdicionarTexto();
        //this.frmPaint.atualizarCanvasNecessitandoRedesenhar();
    }

    public void selectCursorOperacao() {

        this.painelDesenho.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        if(this.frmPaint.getBtnSelectCursor().isSelected()) {

            this.frmPaint.ativarBtnSelectCursor();
            this.painelDesenho.setOperacaoAtual(PainelDesenho.OPERACAO_SELECIONAR_OBJETO);
        }
        else {

            this.frmPaint.atualizarCanvas();
            this.frmPaint.desativarBotoesOperacao();
        }
    }

    public void saveImageOperacao() {

        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory( new File( "./") );

        int answer = chooser.showSaveDialog(this.frmPaint);

        if (answer == JFileChooser.APPROVE_OPTION) {

            File fileName = new File( chooser.getSelectedFile( ) + ".jpg" );

            if(fileName == null) {
                return;
            }

            if(fileName.exists()) {

                answer = JOptionPane.showConfirmDialog(this.frmPaint, "Substituir o arquivo já existente. ", "Salvar Imagem", JOptionPane.INFORMATION_MESSAGE);

                if (answer == JOptionPane.NO_OPTION)
                    return;
            }

            BufferedImage viewImage = this.painelDesenho.getImagem();

            try {

                //this.controlePaint.exportViewToImage(fileName, viewImage);
                JOptionPane.showMessageDialog(this.frmPaint, "Imagem salva !", "Salvar imagem", JOptionPane.INFORMATION_MESSAGE);
            }
            catch(Exception ex) {

                JOptionPane.showMessageDialog(this.frmPaint, "Erro ao salvar a imagem, tente novamente.", "Salvar Imagem", JOptionPane.INFORMATION_MESSAGE);
                ex.printStackTrace();
            }
        }
    }

    public void startAdicionarImagemOperacao() {

    	JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new FileFilter() {

            public String getDescription() {
                return "Imagens (*.jpg, *.png, *.bmp)";
            }

            public boolean accept(File f) {

                if (f.isDirectory()) {

                    return true;
                }
                else {

                    return ((f.getName().toLowerCase().endsWith(".jpg")) || (f.getName().toLowerCase().endsWith(".png")) || (f.getName().toLowerCase().endsWith(".bmp")) );
                }
            }
        });

        int returnVal = fileChooser.showOpenDialog(this.frmPaint);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File fileSelected = fileChooser.getSelectedFile();

            try {

                int totalImages = this.controlePaint.getPaint().totalImagens() + 1;

                this.controlePaint.adicionarImagem2D("Imagem " + totalImages, fileSelected.getAbsolutePath());
                JOptionPane.showMessageDialog(this.frmPaint, "Imagem adicionada com sucesso !", "Adicionar Imagem", JOptionPane.INFORMATION_MESSAGE);
                this.frmPaint.atualizarCanvas();
            }
            catch(IOException ex) {

                ex.printStackTrace();
                JOptionPane.showMessageDialog(this.frmPaint, "Erro ao adicionar a imagem, tente novamente.", "Adicionar Imagem", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void removeImageOperacao() {

        int answer = JOptionPane.showConfirmDialog(this.frmPaint, "Deseja remover a imagem selecionada ?", "Remover Imagem", JOptionPane.YES_NO_OPTION);

        if(answer == JOptionPane.YES_OPTION) {

            Imagem2D imagemAtual = this.painelDesenho.getImagemAtual();

            JOptionPane.showMessageDialog(this.frmPaint, "Imagem removida com sucesso !", "Remover Imagem", JOptionPane.INFORMATION_MESSAGE);
            this.painelDesenho.setImagemAtual(null);
            this.controlePaint.removerImagem(imagemAtual);
            this.frmPaint.atualizarCanvasNecessitandoRedesenhar();
            this.frmPaint.disableMenuItemRemoverImagem();
            this.frmPaint.disableButtonRemoverImagem();
        }
    }

    public void removeObjectOperacao() {

        Figura2D figuraAtual = painelDesenho.getFiguraAtual();

        this.controlePaint.removerFigura(figuraAtual);
        this.painelDesenho.setFiguraAtual(null);
        this.frmPaint.disableJcbFilled();
        this.frmPaint.disableButtonRemoverFigura();
        this.frmPaint.atualizarCanvas();
    }
    
    public void selectDefaultColorOperacao(JButton btnColor) {

        Figura2D figuraAtual = this.painelDesenho.getFiguraAtual();
        Color color = btnColor.getForeground();

        this.frmPaint.getLblCorSelecionada().setForeground(color);
        this.frmPaint.getLblCorSelecionada().setBackground(color);

        if(figuraAtual != null) {

            figuraAtual.setCor(color);
            this.frmPaint.atualizarCanvas();
        }
    }

    public void selectColorOperacao() {

        JColorChooser colorChooser = new JColorChooser(Color.RED);
        Color color = colorChooser.showDialog(this.frmPaint, "Selecionar Color", Color.RED);
        Figura2D figuraAtual = this.painelDesenho.getFiguraAtual();

        this.frmPaint.getLblCorSelecionada().setForeground(color);
        this.frmPaint.getLblCorSelecionada().setBackground(color);

        if(figuraAtual != null) {

            figuraAtual.setCor(color);
            this.frmPaint.atualizarCanvas();
        }

        this.frmPaint.atualizarCanvas();
    }

    /*public void printViewOperacao() {

        try {

            this.controlePaint.printView();
        }
        catch(PrinterException ex) {

            JOptionPane.showMessageDialog(this.frmPaint, "Erro ao realizar a impressão, serviço indisponível.", "Imprimir Mapa", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mapExportOperacao() {

        try {

            BufferedImage viewImage = this.painelDesenho.getBufferDrawing();
            String filePath = this.frmExportarImagem.getTxtDirectory().getText() + "\\" + this.frmExportarImagem.getTxtFilename().getText();
            File fileName = null;

            if(this.frmExportarImagem.getJrbBMPImage().isSelected()) {

                if(!filePath.endsWith(".bmp")) {

                    filePath += ".bmp";
                }

                fileName = new File(filePath);
                this.controlePaint.exportMapToBMP(fileName, viewImage);
                JOptionPane.showMessageDialog(this.frmExportarImagem, "Mapa exportado com sucesso para o formato BMP !", "Exportar Mapa", JOptionPane.INFORMATION_MESSAGE);
            }
            else if(this.frmExportarImagem.getJrbJPGImage().isSelected()) {

                if(!filePath.endsWith(".jpg")) {

                    filePath += ".jpg";
                }

                fileName = new File(filePath);
                this.controlePaint.exportMapToJPG(fileName, viewImage);
                JOptionPane.showMessageDialog(this.frmExportarImagem, "Mapa exportado com sucesso para o formato JPG !", "Exportar Mapa", JOptionPane.INFORMATION_MESSAGE);
            }
            else if(this.frmExportarImagem.getJrbPNGImage().isSelected()) {

                if(!filePath.endsWith(".png")) {

                    filePath += ".png";
                }

                fileName = new File(filePath);
                this.controlePaint.exportMapToPNG(fileName, viewImage);
                JOptionPane.showMessageDialog(this.frmExportarImagem, "Mapa exportado com sucesso para o formato PNG !", "Exportar Mapa", JOptionPane.INFORMATION_MESSAGE);
            }
            else if(this.frmExportarImagem.getJrbPDFFile().isSelected()) {

                if(!filePath.endsWith(".pdf")) {

                    filePath += ".pdf";
                }

                fileName = new File(filePath);
                this.controlePaint.exportMapToPDF(fileName, viewImage);
                JOptionPane.showMessageDialog(this.frmExportarImagem, "Mapa exportado com sucesso para o formato PDF !", "Exportar Mapa", JOptionPane.INFORMATION_MESSAGE);
            }

            this.frmExportarImagem.closeFrmExportarImagem();
        }
        catch(IOException ex) {

            ex.printStackTrace();
            JOptionPane.showMessageDialog(this.frmExportarImagem, "Erro ao exportar o mapa, tente novamente.", "Exportar Mapa", JOptionPane.ERROR_MESSAGE);
        }
        catch(DocumentException ex) {

            ex.printStackTrace();
            JOptionPane.showMessageDialog(this.frmExportarImagem, "Erro ao exportar o mapa, tente novamente.", "Exportar Mapa", JOptionPane.ERROR_MESSAGE);
        }
    }*/

    public void capturarImagem() {
    	
    	VideoCapture camera = new VideoCapture(0);
    	
    	if(!camera.isOpened()){
    		System.out.println("Error");
    	}
    	else {
    		
    		Mat frame = new Mat();
    		int i = 0;
    	    while(true){
    	    	if (camera.read(frame)){
    	    		System.out.println("Frame Obtained");
    	    		System.out.println("Captured Frame Width " + 
    	    		frame.width() + " Height " + frame.height());
    	    		Highgui.imwrite("camera" + i + ".jpg", frame);
    	    		System.out.println("OK");
    	    		//i++;
    	    		Mat2Image mat2Img = new Mat2Image();
    	        	camera.read(mat2Img.mat);
    	            BufferedImage imagem = mat2Img.getImage(mat2Img.mat);
    	            painelDesenho.setImagemCapturada(imagem);
    	            frmPaint.atualizarCanvasNecessitandoRedesenhar();
    	    		break;
    	    	}
    	    }	
    	}
    	
    	camera.release();
    }
    
    public void actionPerformed(ActionEvent e) {

        if( (e.getSource() == this.frmPaint.getBtnCorVermelho()) || (e.getSource() == this.frmPaint.getBtnCorVerde()) || (e.getSource() == this.frmPaint.getBtnCorAzul()) || (e.getSource() == this.frmPaint.getBtnCorAmarelo()) || (e.getSource() == this.frmPaint.getBtnCorLaranja())) {

            this.selectDefaultColorOperacao(((JButton)e.getSource()));
        }
        else if(e.getSource() == this.frmPaint.getBtnSelecionarCor()) {

            this.selectColorOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnImprimir()) {

            //this.printViewOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnSelectCursor()) {

            this.selectCursorOperacao();
        }
        else if(e.getSource() == this.frmPaint.getMenuItemCursorSelect()) {

            this.frmPaint.desativarBotoesOperacao();
            this.frmPaint.ativarBtnSelectCursor();
            this.selectCursorOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnAdicionarLinha()) {

            this.ativarOuDesativarAdicionarLinhaOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnAdicionarCirculo()) {

            this.ativarOuDesativarAddCircleOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnAdicionarRetangulo()) {

            this.ativarOuDesativarAdicionarRetanguloOperacao();
        }
        else if(e.getSource() == this.frmPaint.getBtnAdicionarPoligono()) {

            this.ativarOuDesativarAdicionarPoligonoOperacao();
        }
        else if(e.getSource() == this.frmPaint.getMenuItemAdicionarLinha()) {

            this.frmPaint.desativarBotoesOperacao();
            this.frmPaint.ativarBtnAdicionarLinha();
            this.ativarOuDesativarAdicionarLinhaOperacao();
        }
        else if(e.getSource() == this.frmPaint.getMenuItemAdicionarCirculo()) {

            this.frmPaint.desativarBotoesOperacao();
            this.frmPaint.ativarBtnAdicionarCirculo();;
            this.ativarOuDesativarAddCircleOperacao();
        }
        else if(e.getSource() == this.frmPaint.getMenuItemAdicionarRetangulo()) {

            this.frmPaint.desativarBotoesOperacao();
            this.frmPaint.ativarBtnAdicionarRetangulo();
            this.ativarOuDesativarAdicionarRetanguloOperacao();
        }
        else if(e.getSource() == this.frmPaint.getMenuItemAdicionarPoligono()) {

            this.frmPaint.desativarBotoesOperacao();
            this.frmPaint.ativarBtnAdicionarPoligono();
            this.ativarOuDesativarAdicionarPoligonoOperacao();
        }
        else if( (e.getSource() == this.frmPaint.getBtnAdicionarTexto()) || (e.getSource() == this.frmPaint.getMenuItemAdicionarTexto()) ) {

            this.frmAdicionarTexto.abrirFrmAdicionarTexto();
        }
        else if( (e.getSource() == this.frmPaint.getBtnRemoverFigura()) || (e.getSource() == this.frmPaint.getMenuItemRemoverFigura()) ) {

            this.removeObjectOperacao();
        }
        else if(e.getSource() == this.frmAdicionarTexto.getBtnAdicionarTexto()) {

            this.addTextOperacao();
        }
        else if(e.getSource() == this.frmAdicionarTexto.getBtnCancelar()) {

            this.frmAdicionarTexto.fecharFrmAdicionarTexto();
        }
        else if( (e.getSource() == this.frmPaint.getMenuItemAdicionarImagem()) || (e.getSource() == this.frmPaint.getBtnAdicionarImagem()) ) {

            this.startAdicionarImagemOperacao();
        }
        else if( (e.getSource() == this.frmPaint.getBtnSaveImagem()) || (e.getSource() == this.frmPaint.getMenuItemExportarImagem())) {

            //this.frmExportarImagem.openFrmExportarImagem();
        }
        else if(e.getSource() == this.frmExportarImagem.getBtnDirectorySelect()) {

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(false);

            int returnVal = fileChooser.showOpenDialog(this.frmExportarImagem);

            if (returnVal == JFileChooser.APPROVE_OPTION) {

                this.frmExportarImagem.getTxtDirectory().setText(fileChooser.getCurrentDirectory().getAbsolutePath());
            }
        }
        else if(e.getSource() == this.frmExportarImagem.getBtnMapExport()) {

            //this.mapExportOperacao();
        }
        else if(e.getSource() == this.frmExportarImagem.getBtnCancel()) {

            //this.frmExportarImagem.closeFrmExportarImagem();
        }
        else if( (e.getSource() == this.frmPaint.getBtnFechar()) || (e.getSource() == this.frmPaint.getMenuItemFechar()) ) {

            //this.frmPaint.closeFrmPaint();
        }
        else if(e.getSource() == this.frmPaint.getBtnCapturarImagem()) {
        	
        	capturarImagem();
        }
    }

    public void itemStateChanged(ItemEvent e) {

        if(e.getSource() == this.frmPaint.getJcbFilled()) {

            Figura2D activeShape = this.painelDesenho.getFiguraAtual();
            
            if(this.frmPaint.getJcbFilled().isEnabled()) {

                if(e.getStateChange() == ItemEvent.SELECTED) {

                    activeShape.setPreenchido(true);
                }
                else {

                    activeShape.setPreenchido(false);
                }
            }

            this.frmPaint.atualizarCanvas();
        }
    }

    public void stateChanged(ChangeEvent e) {

        if(e.getSource() == this.frmPaint.getBtnSelectCursor()) {

            this.selectCursorOperacao();
        }
    }
}