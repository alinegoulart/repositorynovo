package controle;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class ImpressaoPaint implements Printable {

    private BufferedImage viewCanvasImage;

    public ImpressaoPaint(BufferedImage viewCanvasImage) {

        this.viewCanvasImage = viewCanvasImage;
    }

    public void setViewCanvasImage(BufferedImage viewCanvasImage) {
        this.viewCanvasImage = viewCanvasImage;
    }

    public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {

        g.translate((int) (-pf.getImageableX()), (int) (-pf.getImageableY()));
        
        if (pageIndex == 0) {

            g.drawImage(this.viewCanvasImage, 0, 0, null);
            return Printable.PAGE_EXISTS;
        }
        return Printable.NO_SUCH_PAGE;
    }
}