package utilidades;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class MathUtility {

    public static int getStringWidth(Font font, String value) {

        BufferedImage buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = buffer.createGraphics();
        FontMetrics metrics = g2d.getFontMetrics(font);
        Rectangle2D bounds = metrics.getStringBounds(value, null);
        int widthInPixels = (int) bounds.getWidth();

        return widthInPixels;
    }

    public static int getStringHeight(Font font, String value) {

        BufferedImage buffer = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = buffer.createGraphics();
        FontMetrics metrics = g2d.getFontMetrics(font);
        Rectangle2D bounds = metrics.getStringBounds(value, null);
        int widthInPixels = (int) bounds.getHeight();

        return widthInPixels;
    }
    
    public static Point2D.Float getTextPosition(TextLayout layout, float xPosition, float yPosition) {

        float x = xPosition;
        float y = yPosition; /* Position of top left corner to draw the TextLayout. */
        float ox = 0.0f;
        float oy = 0.0f; /* Position of origin to draw the TextLayout. */
        Rectangle2D bounds = layout.getBounds();

        ox = x - (float)bounds.getX();
        oy = y - (float)bounds.getY();

        return new Point2D.Float(ox, oy);
    }
}